import logging
from functools import wraps


def log_call_and_exception(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        LOGGER = logging.getLogger("todo_lib")
        LOGGER.info(f"function call: {f.__name__}")
        LOGGER.debug(f"args: {args}")
        LOGGER.debug(f"kwargs: {kwargs}")
        try:
            res = f(*args, **kwargs)
            LOGGER.debug(f"result: {res!r}")
            return res
        except Exception as e:
            LOGGER.error(e)
            LOGGER.exception(e)
            raise
    return wrapper


def get_logger():
    return logging.getLogger("todo_lib")
