import datetime
import enum

from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Integer,
    Interval,
    String,
    Table,
    create_engine,
    event
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import (
    backref,
    relationship,
    sessionmaker,
    scoped_session
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import (
    select,
    func
)

from todo_lib.logging import log_call_and_exception


class DeltaType(enum.Enum):
    YEAR = 1
    MONTH = 2
    DAY = 3
    HOUR = 4


class RuleEndCondition(enum.Enum):
    DATE = 1
    COUNT = 2
    ENDLESS = 3


class Priority(enum.Enum):
    CRITICAL = 1
    IMPORTANT = 2
    NORMAL = 3
    LOW = 4


Base = declarative_base()

task_label_association = Table(
    "task_label",
    Base.metadata,
    Column("task_id", Integer, ForeignKey("task.id")),
    Column("label_id", Integer, ForeignKey("label.id")),
)


class Project(Base):
    __tablename__ = "project"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    user = Column(String)

    tasks = relationship("Task", back_populates="project")

    def __init__(self, name, user):
        self.name = name
        self.user = user

    def __str__(self):
        return "\n".join([
            f"[{self.id}] {self.name}",
            f"Tasks: {len(self.tasks)}"
        ])

    def __repr__(self):
        return f"Project(name={self.name!r}, user={self.user!r})"


class Task(Base):
    __tablename__ = "task"

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey("task.id"))
    tree_root_id = Column(Integer, ForeignKey("task.id"))
    name = Column(String)
    description = Column(String)
    completed = Column(Boolean)
    date = Column(DateTime)
    archived = Column(Boolean)
    priority = Column(Enum(Priority))
    created = Column(DateTime,
                     default=datetime.datetime.utcnow, nullable=False)
    updated = Column(DateTime,
                     default=datetime.datetime.utcnow, nullable=False)

    user_relations = relationship("TaskUserRelation")

    project_id = Column(Integer, ForeignKey("project.id"))
    project = relationship("Project", back_populates="tasks")

    labels = relationship(
        "Label", secondary=task_label_association, back_populates="tasks")

    reminder = relationship("Reminder", uselist=False, back_populates="task")

    repeat_rule_id = Column(Integer, ForeignKey("repeat_rule.id"))
    repeat_rule = relationship("RepeatRule", back_populates="tasks")

    def __init__(
        self,
        name,
        priority=Priority.LOW,
        parent_id=None,
        description="",
        date=None,
        tree_root_id=None
    ):
        self.name = name
        self.priority = priority
        self.description = description
        self.date = date
        self.parent_id = parent_id
        self.tree_root_id = tree_root_id
        self.completed = False
        self.subtasks = set()
        self.archived = False

    @hybrid_property
    def users(self):
        return [rel.user for rel in self.user_relations]

    @hybrid_property
    def priority_value(self):
        return self.priority.value

    @hybrid_property
    def priority_name(self):
        return self.priority.name.lower()

    @hybrid_property
    def labels_name(self):
        return [label.name for label in self.labels]

    def __str__(self):
        if self.date is not None:
            date_str = self.date.strftime("%Y-%m-%d %H:%M")
        return "\n".join([
            f"[{self.id}] {self.name}",
            f"priority: {self.priority.name.lower()}",
            f"description: {self.description}",
            f"archived: {self.archived}",
            f"completed: {self.completed}",
            f"date: {date_str if self.date else None}",
            f"labels: {', '.join(label.name for label in self.labels) or '[]'}",
            f"project: {self.project.name if self.project else None}",
            f"users: {', '.join(self.users) or '[]'}"
        ])

    def __repr__(self):
        return ", ".join([
            f"Task(name={self.name!r}",
            f"priority={self.priority!r}",
            f"description={self.description!r}",
            f"date={self.date!r}",
            f"archived={self.archived!r}",
            f"completed={self.completed!r})"
        ])


@event.listens_for(Task, "before_update")
def task_before_update(mapper, connection, target):
    target.updated = datetime.datetime.utcnow()


@event.listens_for(Task, "load")
def task_before_attach(target, context):
    target.subtasks = set()


class TaskUserRelation(Base):
    __tablename__ = "task_user"

    id = Column(Integer, primary_key=True)
    task = Column(Integer, ForeignKey("task.id"))
    user = Column(String)


class Label(Base):
    __tablename__ = "label"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    user = Column(String)
    predefined = Column(Boolean)

    tasks = relationship(
        "Task",
        secondary=task_label_association,
        back_populates="labels"
    )

    def __init__(self, name, user, predefined=False):
        self.name = name
        self.user = user
        self.predefined = predefined

    def __repr__(self):
        return f"Label(name={self.name!r}, user={self.user!r})"


class Reminder(Base):
    __tablename__ = "reminder"
    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    user = Column(String)

    task_id = Column(Integer, ForeignKey("task.id"))
    task = relationship("Task", back_populates="reminder")

    def __init__(self, task, date, user):
        self.date = date
        self.task = task
        self.user = user

    def __str__(self):
        return "\n".join([
            f"Reminder [{self.id}]",
            f"Date: {self.date}",
            f"Task: {self.task_id}"
        ])

    def __repr__(self):
        return f"Reminder(task={self.task!r}, date={self.date!r})"


class MetaTask(Base):
    __tablename__ = "meta_task"

    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey("task.id"))
    name = Column(String)
    description = Column(String)
    priority = Column(Enum(Priority))

    project_id = Column(Integer, ForeignKey("project.id"))
    project = relationship("Project")

    repeat_rule = relationship("RepeatRule",
                               back_populates="meta_task", uselist=False)

    def __init__(self, parent_id, name, description, priority):
        self.parent_id = parent_id
        self.name = name
        self.description = description
        self.priority = priority

    def __repr__(self):
        return ", ".join([
            f"MetaTask(parent_id={self.parent_id!r}",
            f"name={self.name!r}",
            f"priority={self.priority!r}",
            f"description={self.description!r})"
        ])


class RepeatRule(Base):
    __tablename__ = "repeat_rule"

    id = Column(Integer, primary_key=True)
    start = Column(DateTime)
    delta_type = Column(Enum(DeltaType))
    delta_quantity = Column(Integer)
    end_date = Column(DateTime)
    last_activated = Column(DateTime)
    total_count = Column(Integer)
    completed_count = Column(Integer)
    end_by = Column(Enum(RuleEndCondition))
    user = Column(String)

    tasks = relationship("Task", back_populates="repeat_rule")
    meta_task_id = Column(Integer, ForeignKey("meta_task.id"))
    meta_task = relationship("MetaTask", back_populates="repeat_rule")

    def __init__(self, meta_task, start, delta_type, delta_quantity,
                 user, end_condition=None, end_date=None,
                 total_count=None):
        if end_condition is RuleEndCondition.COUNT and total_count is None:
            raise ValueError()
        elif end_condition is RuleEndCondition.DATE and end_date is None:
            raise ValueError()

        self.meta_task = meta_task
        self.start = start
        self.delta_type = delta_type
        self.delta_quantity = delta_quantity
        self.user = user
        self.end_by = end_condition
        self.end_date = end_date
        self.total_count = total_count
        self.last_activated = start
        self.completed_count = 0

    def __repr__(self):
        return ", ".join([
            f"RepeatRule(delta_type={self.delta_type!r}",
            f"delta_quantity={self.delta_quantity!r}",
            f"end_date={self.end_date!r}",
            f"user={self.user}"
            f"meta_task={self.meta_task!r})"
        ])

@log_call_and_exception
def init_session(db_type, db_path):
    connection = f"{db_type}:////{db_path}"
    engine = create_engine(
        connection,
        connect_args={'check_same_thread': False}
    )
    Session = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    return scoped_session(Session)
