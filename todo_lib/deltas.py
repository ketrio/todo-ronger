from dateutil.relativedelta import relativedelta

def get_delta(delta_type, quantity):
    return {
        "YEAR": relativedelta(years=quantity),
        "MONTH": relativedelta(months=quantity),
        "DAY": relativedelta(days=quantity),
        "HOUR": relativedelta(hours=quantity)
    }[delta_type.name]
