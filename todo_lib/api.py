from contextlib import contextmanager
import datetime
import warnings

import sqlalchemy
from sqlalchemy import (
    create_engine,
    or_,
    and_
)
from sqlalchemy.orm import (
    sessionmaker,
    aliased,
    scoped_session
)
from sqlalchemy.sql.expression import case

from todo_lib.models import (
    Base,
    Label,
    MetaTask,
    Project,
    Reminder,
    RepeatRule,
    RuleEndCondition,
    Task,
    TaskUserRelation,
    Priority,
    DeltaType
)
from todo_lib.deltas import get_delta
from todo_lib.exceptions import (
    CircularDependencyError,
    ObjectNotFound,
    TaskAlreadyShared,
    ActionAlreadyApplied,
    TaskAlreadyAttached,
    TaskAlreadyCompleted
)
from todo_lib.logging import (
    log_call_and_exception,
    get_logger
)

Logger = get_logger()


class Api:
    @log_call_and_exception
    def __init__(self, session):
        self.session = session

    @log_call_and_exception
    def create_task(
            self,
            user,
            name,
            priority="low",
            description="",
            date=None,
            parent_id=None
    ):
        try:
            priority = Priority[priority.upper()]
        except KeyError as e:
            raise KeyError("No such priority") from e

        parent = None
        tree_root_id = None
        if parent_id is not None:
            try:
                parent = self.get_task(user=user, task_id=parent_id)
                tree_root_id = parent.tree_root_id
            except ObjectNotFound as e:
                raise ObjectNotFound("No parent task found") from e

        task = Task(
            name=name,
            description=description,
            date=date,
            parent_id=parent_id,
            tree_root_id=tree_root_id,
            priority=priority
        )
        task.user_relations.append(
            TaskUserRelation(task=task, user=user)
        )

        task.tree_root_id = task.id
        self.session.add(task)
        return task

    @log_call_and_exception
    def get_task(
        self,
        user,
        task_id=None
    ):
        try:
            return (self.session.query(Task)
                    .join(Task.user_relations)
                    .filter(TaskUserRelation.user == user)
                    .filter(Task.id == task_id)
                    .one())
        except sqlalchemy.orm.exc.NoResultFound as e:
            raise ObjectNotFound("No task found") from e

    @log_call_and_exception
    def get_subtasks(self, user, task_id):
        Subtask = aliased(Task, name="subtask")
        return (self.session.query(Subtask)
                .join(Task, Subtask.parent_id == Task.id)
                .join(Task.user_relations)
                .filter(TaskUserRelation.user == user)
                .filter(Task.id == task_id)
                .all())

    @log_call_and_exception
    def get_subtask_tree(self, user, task):
        task.subtasks = self.get_subtasks(user=user, task_id=task.id)
        for subtask in task.subtasks:
            self.get_subtask_tree(user=user, task=subtask)
        return task

    @log_call_and_exception
    def get_all_trees(self, user, archived=None, completed=None):
        roots = self.get_tasks(
            user=user,
            has_parent=False,
            archived=archived,
            completed=completed
        )
        for root in roots:
            self.get_subtask_tree(user=user, task=root)
        return roots

    @log_call_and_exception
    def get_tasks(
        self,
        user,
        name=None,
        parent_id=None,
        tree_root_id=None,
        has_parent=None,
        description=None,
        has_description=None,
        completed=None,
        archived=None,
        date=None,
        has_date=None,
        project_id=None
    ):
        condition = and_(
            or_(parent_id is None, Task.parent_id == parent_id),
            or_(tree_root_id is None, Task.tree_root_id == tree_root_id),
            or_(has_parent is None, (Task.parent_id != None) == has_parent),
            or_(name is None, Task.name == name),
            or_(description is None, Task.description == description),
            or_(has_description is None, (Task.description != None) == has_description),
            or_(completed is None, Task.completed == completed),
            or_(archived is None, Task.archived == archived),
            or_(date is None, Task.date == date),
            or_(has_date is None, (Task.date != None) == has_date),
            or_(project_id is None, Task.project_id == project_id)
        )
        enums = Task.priority.type.enums
        whens = {priority: index for index, priority in enumerate(enums)}
        sort_logic = case(value=Task.priority, whens=whens).label("priority")
        return (self.session.query(Task)
                .join(Task.user_relations)
                .filter(TaskUserRelation.user == user)
                .filter(condition)
                .order_by(sort_logic)
                .all())

    @log_call_and_exception
    def search_tasks(self, user, query):
        return (self.session.query(Task)
                .join(Task.user_relations)
                .filter(TaskUserRelation.user == user)
                .filter(Task.name.like(f"%{query}%"))
                .all())

    @log_call_and_exception
    def add_subtask(self, user, parent_task_id, subtask_id):
        parent_task = self.get_task(user=user, task_id=parent_task_id)
        subtasks = self.get_subtasks(user=user, task_id=parent_task_id)
        subtask = self.get_task(user=user, task_id=subtask_id)

        if subtask in subtasks:
            warnings.warn(
                "Task is already in subtasks of parent task.",
                TaskAlreadyAttached
            )
            return

        if parent_task.tree_root_id != subtask_id:
            subtask.parent_id = parent_task_id
            subtask.tree_root_id = (parent_task.tree_root_id or
                                    parent_task_id)
            return
        raise (
            CircularDependencyError(
                "Subtask circular depedency detected"
            )
        )

    @log_call_and_exception
    def share_task(self, user, task_id, collaborator):
        task = self.get_task(user=user, task_id=task_id)
        if collaborator in task.users:
            warnings.warn(
                "Task is already shared with this user.",
                TaskAlreadyShared
            )
            return
        collaborator_relation = TaskUserRelation(
            task=task,
            user=collaborator
        )
        task.user_relations.append(collaborator_relation)

    @log_call_and_exception
    def complete_task(self, user, task_id, complete_subtasks=True):
        task = self.get_task(user=user, task_id=task_id)
        if task.completed:
            warnings.warn(
                "Task is already completed.",
                TaskAlreadyCompleted
            )
        task.completed = True
        if complete_subtasks:
            self.complete_subtasks(user=user, task_id=task_id)

    @log_call_and_exception
    def complete_subtasks(self, user, task_id):
        subtasks = self.get_subtasks(user=user, task_id=task_id)
        for subtask in subtasks:
            subtask.completed = True
            self.complete_subtasks(user=user, task_id=subtask.id)

    @log_call_and_exception
    def archive_task(self, user, task_id, archive_subtasks=True):
        task = self.get_task(user=user, task_id=task_id)
        if task.archived:
            warnings.warn(
                "Task is already archived.",
                TaskAlreadyCompleted
            )
        task.archived = True
        if archive_subtasks:
            self.archive_subtasks(user=user, task_id=task_id)

    @log_call_and_exception
    def archive_subtasks(self, user, task_id):
        subtasks = self.get_subtasks(user=user, task_id=task_id)
        for subtask in subtasks:
            subtask.archived = True
            self.archive_subtasks(user=user, task_id=subtask.id)

    @log_call_and_exception
    def create_project(self, user, name):
        project = Project(name=name, user=user)
        self.session.add(project)
        return project

    @log_call_and_exception
    def get_project(self, user, project_id=None, name=None):
        try:
            return (self.session.query(Project)
                    .filter_by(user=user)
                    .filter(or_(project_id is None, Project.id == project_id))
                    .filter(or_(name is None, Project.name == name))
                    .one())
        except sqlalchemy.orm.exc.NoResultFound as e:
            raise ObjectNotFound("No result found") from e

    @log_call_and_exception
    def get_projects(self, user, name=None):
        return (self.session.query(Project)
                .filter_by(user=user)
                .filter(or_(name is None, Project.name == name))
                .all())

    @log_call_and_exception
    def create_reminder(self, user, task_id, date):
        try:
            task = self.get_task(user=user, task_id=task_id)
        except ValueError as e:
            raise ValueError("No task found with given id") from e

        reminder = Reminder(task=task, date=date, user=user)
        self.session.add(reminder)
        return reminder

    @log_call_and_exception
    def get_reminder(
            self,
            user,
            reminder_id
        ):
        try:
            return (self.session.query(Reminder)
                    .filter_by(user=user)
                    .filter(
                        or_(
                            reminder_id is None,
                            Reminder.id == reminder_id
                        )
                    )
                    .one())
        except sqlalchemy.orm.exc.NoResultFound as e:
            raise ObjectNotFound("No reminder found") from e

    @log_call_and_exception
    def get_reminders(
            self,
            user,
            date=None,
            task_id=None
        ):
        return (self.session.query(Reminder)
                .filter_by(user=user)
                .filter(
                    or_(
                        date is None,
                        Reminder.date == date
                    )
                )
                .filter(
                    or_(
                        task_id is None,
                        Reminder.task_id == id
                    )
                )
                .all())

    @log_call_and_exception
    def create_label(self, user, name):
        label = Label(name=name, user=user)
        self.session.add(label)
        return label

    @log_call_and_exception
    def get_label(
        self,
        user,
        label_id
    ):
        try:
            return (self.session.query(Label)
                    .filter_by(user=user)
                    .filter(
                        or_(label_id is None, Label.id == label_id),
                    )
                    .one())
        except sqlalchemy.orm.exc.NoResultFound as e:
            raise ObjectNotFound("No label found") from e

    @log_call_and_exception
    def get_labels(
            self,
            user,
            name=None,
            predefined=None
        ):
        return (self.session.query(Label)
                .filter_by(user=user)
                .filter(or_(name is None, Label.name == name))
                .filter(
                    or_(
                        predefined is None,
                        Label.predefined == predefined
                    )
                )
                .all())

    @log_call_and_exception
    def create_repeat_rule(
        self,
        user,
        task_id,
        delta_type,
        delta_quantity,
        end_date=None,
        count=None
    ):
        task = self.get_task(user=user, task_id=task_id)
        if task.date is None:
            raise ValueError("Target task date must be defined first")
        if task.repeat_rule is not None:
            raise ValueError("Task already has repeat rule")

        if end_date is not None and count is not None:
            delta = get_delta(delta_type, delta_quantity)
            if task.date + delta * count < end_date:
                end_condition = RuleEndCondition.COUNT
            else:
                end_condition = RuleEndCondition.DATE
        elif end_date is not None:
            end_condition = RuleEndCondition.DATE
        elif count is not None:
            end_condition = RuleEndCondition.COUNT
        else:
            end_condition = RuleEndCondition.ENDLESS

        try:
            delta_type = DeltaType[delta_type.upper()]
        except KeyError as e:
            raise KeyError("No such priority") from e

        meta_task = MetaTask(
            parent_id=task.parent_id,
            name=task.name,
            description=task.description,
            priority=task.priority
        )
        repeat_rule = RepeatRule(
            meta_task=meta_task,
            start=task.date,
            delta_type=delta_type,
            delta_quantity=delta_quantity,
            end_condition=end_condition,
            end_date=end_date,
            total_count=count,
            user=user
        )
        task.repeat_rule = repeat_rule
        self.session.add(meta_task, repeat_rule)
        return repeat_rule

    @log_call_and_exception
    def get_repeat_rule(
        self,
        user,
        rule_id
    ):
        try:
            return (self.session.query(RepeatRule)
                    .filter(and_(
                        RepeatRule.user == user,
                        RepeatRule.id == rule_id
                    ))
                    .one())
        except sqlalchemy.orm.exc.NoResultFound as e:
            raise ObjectNotFound("No repeat rule found") from e

    @log_call_and_exception
    def get_repeat_rules(
        self,
        user
    ):
        return (self.session.query(RepeatRule)
                .filter(RepeatRule.user == user)
                .all())

    @log_call_and_exception
    def check_repeat_rules(self, user=None):
        active_repeat_rules = []
        rules_end_by_date = (
            self.session.query(RepeatRule)
            .filter(or_(user is None, RepeatRule.user == user))
            .filter_by(end_by=RuleEndCondition.DATE)
            .all()
        )
        Logger.info(f"Found {len(rules_end_by_date)} active date rules")
        rules_end_by_count = (
            self.session.query(RepeatRule)
            .filter(or_(user is None, RepeatRule.user == user))
            .filter_by(end_by=RuleEndCondition.COUNT)
            .all()
        )
        Logger.info(f"Found {len(rules_end_by_count)} active count rules")
        endless_rules = (
            self.session.query(RepeatRule)
            .filter(or_(user is None, RepeatRule.user == user))
            .filter_by(end_by=RuleEndCondition.ENDLESS)
            .all()
        )
        Logger.info(f"Found {len(endless_rules)} active endless rules")

        for date_rule in rules_end_by_date:
            delta = get_delta(
                date_rule.delta_type,
                date_rule.delta_quantity
            )
            next_activation = date_rule.last_activated + delta
            if (next_activation < datetime.datetime.now() and
                    next_activation < date_rule.end_date):
                active_repeat_rules.append(date_rule)

        for count_rule in rules_end_by_count:
            delta = get_delta(
                count_rule.delta_type,
                count_rule.delta_quantity
            )
            next_activation = count_rule.last_activated + delta
            if (next_activation < datetime.datetime.now()
                    and (count_rule.completed_count
                         < count_rule.total_count)
                    ):
                active_repeat_rules.append(count_rule)

        for endless_rule in endless_rules:
            delta = get_delta(
                endless_rule.delta_type,
                endless_rule.delta_quantity
            )
            next_activation = endless_rule.last_activated + delta
            if next_activation < datetime.datetime.now():
                active_repeat_rules.append(endless_rule)

        return active_repeat_rules

    @log_call_and_exception
    def apply_repeat_rules(self, rules):
        tasks_created = 0
        for rule in rules:
            delta = get_delta(rule.delta_type, rule.delta_quantity)
            next_activation = rule.last_activated + delta
            while next_activation < datetime.datetime.now():
                if (rule.end_by is RuleEndCondition.COUNT and
                        not rule.completed_count < rule.total_count):
                    break
                if (rule.end_by is RuleEndCondition.DATE and
                        not next_activation < rule.end_date):
                    break
                rule.completed_count += 1
                meta_task = rule.meta_task
                task = self.create_task(
                    user=rule.user,
                    name=meta_task.name,
                    description=meta_task.description,
                    date=next_activation,
                    parent_id=meta_task.parent_id
                )
                self.session.add(task)
                rule.last_activated = next_activation
                next_activation = rule.last_activated + delta
                tasks_created += 1
        Logger.info(f"{tasks_created} planned tasks created")

    def save(self):
        self.session.commit()

    @contextmanager
    def save_after(self):
        try:
            yield self
            self.save()
            Logger.debug("State saved")
        except Exception as e:
            self.abort()
            Logger.exception(e)
            Logger.debug("State aborted")
            raise

    @log_call_and_exception
    def delete(self, obj):
        self.session.delete(obj)

    @log_call_and_exception
    def abort(self):
        self.session.rollback()


def main():
    pass


if __name__ == "__main__":
    main()
