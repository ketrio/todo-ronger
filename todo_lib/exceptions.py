class LibraryError(Exception):
    """Generic error class."""


class CircularDependencyError(LibraryError):
    """Raised by adding subtask when a circular dependency is detected."""

class ObjectNotFound(LibraryError):
    """Raised by getting object which doesn"t exist."""

class DatabaseError(LibraryError):
    """Raised by database command when database error happened."""

class LibraryWarning(Warning):
    """Generic warning class."""

class ActionAlreadyApplied(LibraryWarning):
    """Raised by applying action to object that has already been applied."""

class TaskAlreadyShared(ActionAlreadyApplied):
    """Raised by sharing task with user who already has access to it."""

class TaskAlreadyAttached(ActionAlreadyApplied):
    """Raised by attaching task as subtask when it's already attached."""

class TaskAlreadyCompleted(ActionAlreadyApplied):
    """Raised by completing already completed task."""
