import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import tasks from './reducers/tasks';
import labels from './reducers/labels';

const configureStore = () => createStore(
  combineReducers({ tasks, labels }),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk),
);

export default configureStore;
