import * as types from './types';
import { getAllTasks } from './tasks';
import { server } from '../helpers/config';

const defaultRequestBeginning = `${server.hostname}/${server.defaultUsername}`;

const requestLabels = () => ({
  type: types.REQUEST_LABELS,
});

const receiveLabels = label => ({
  type: types.RECEIVE_LABELS,
  label,
});

const abortLabel = () => ({
  type: types.ABORT_LABEL,
});

export const getLabel = id => (dispatch) => {
  dispatch(requestLabels());
  fetch(`${defaultRequestBeginning}/labels/${id}`, { method: 'GET' })
    .then(response => response.json())
    // eslint-disable-next-line arrow-body-style
    .then((labels) => {
      dispatch(receiveLabels(labels));
    })
    .catch((error) => {
      dispatch(abortLabel());
      console.error(error);
    });
};

export const getAllLabels = () => (dispatch) => {
  dispatch(requestLabels());
  fetch(`${defaultRequestBeginning}/labels/`, { method: 'GET' })
    .then(response => response.json())
    .then(tasks => dispatch(receiveLabels(tasks)))
    .catch((error) => {
      dispatch(abortLabel());
      console.error(error);
    });
};

const postHeaders = new Headers();
postHeaders.append('Content-Type', 'application/json');

export const createLabel = name => (dispatch) => {
  dispatch(requestLabels());
  fetch(
    `${defaultRequestBeginning}/labels/`,
    {
      method: 'POST',
      headers: postHeaders,
      body: JSON.stringify({ name }),
    },
  )
    .then(response => response.json())
    .then(() => dispatch(getAllLabels()), error => console.error(error));
};

// eslint-disable-next-line camelcase
export const addTaskToLabel = (label_id, task_id, method = 'POST') => (dispatch) => {
  dispatch(requestLabels());
  // eslint-disable-next-line camelcase
  fetch(`${defaultRequestBeginning}/labels/${label_id}/tasks/`, {
    method,
    headers: postHeaders,
    body: JSON.stringify({ task_id }),
  })
    // .then(response => response.json())
    // .then((json) => {
    //   console.log(json);
    //   return json;
    // })
    .then(() => {
      dispatch(getAllLabels());
      dispatch(getAllTasks());
    })
    .catch((error) => { console.error(error); });
};
