import * as types from './types';
import { server } from '../helpers/config';

const defaultRequestBeginning = `${server.hostname}/${server.defaultUsername}`;

const requestTask = () => ({
  type: types.REQUEST_TASK,
});

const receiveTask = task => ({
  type: types.RECEIVE_TASK,
  task,
});

const receiveCurrentTask = task => ({
  type: types.RECEIVE_CURRENT_TASK,
  task,
});

const abortTask = () => ({
  type: types.ABORT_TASK,
});

export const getTask = (id, current = false) => (dispatch) => {
  dispatch(requestTask());
  fetch(`${defaultRequestBeginning}/tasks/${id}`, { method: 'GET' })
    .then(response => response.json())
    // eslint-disable-next-line arrow-body-style
    .then((task) => {
      return current ? dispatch(receiveCurrentTask(task)) : dispatch(receiveTask(task));
    })
    .catch((error) => {
      dispatch(abortTask());
      console.error(error);
    });
};

export const getAllTasks = () => (dispatch) => {
  dispatch(requestTask());
  fetch(`${defaultRequestBeginning}/tasks/`, { method: 'GET' })
    .then(response => response.json())
    .then(tasks => dispatch(receiveTask(tasks)))
    .catch((error) => {
      dispatch(abortTask());
      console.error(error);
    });
};

const postHeaders = new Headers();
postHeaders.append('Content-Type', 'application/json');

export const createTask = props => (dispatch) => {
  // dispatch(requestTask());
  // const params = Object.keys(props)
  //   .filter(key => !!props[key])
  //   .map(key => `${key}=${props[key]}`)
  //   .join('&');
  fetch(
    `${defaultRequestBeginning}/tasks/`,
    {
      method: 'POST',
      headers: postHeaders,
      body: JSON.stringify(props),
    },
  )
    .then(() => dispatch(getAllTasks()), error => console.error(error));
};

export const updateTask = ({ id, ...rest }) => (dispatch) => {
  // dispatch(requestTask());
  // const props = { ...rest };
  // Object.keys(props).forEach((key) => {
  //   if (props.key == null) delete props[key];
  // });
  fetch(
    `${defaultRequestBeginning}/tasks/${id}/`,
    {
      method: 'PUT',
      headers: postHeaders,
      body: JSON.stringify(rest),
    },
  )
    .then(() => dispatch(getAllTasks()), error => console.error(error));
};

export const completeTask = id => (dispatch) => {
  // dispatch(requestTask());
  fetch(`${defaultRequestBeginning}/tasks/${id}/complete/`, { method: 'POST' })
    .then(() => dispatch(getAllTasks()))
    .catch((error) => { console.error(error); });
};

export const archiveTask = id => (dispatch) => {
  // dispatch(requestTask());
  fetch(`${defaultRequestBeginning}/tasks/${id}/archive/`, { method: 'POST' })
    .then(() => dispatch(getAllTasks()))
    .catch((error) => { console.error(error); });
};