export const ABORT_TASK = 'ABORT_TASK';
export const REQUEST_TASK = 'REQUEST_TASK';
export const RECEIVE_TASK = 'RECEIVE_TASK';
export const REQUEST_CURRENT_TASK = 'REQUEST_CURRENT_TASK';
export const RECEIVE_CURRENT_TASK = 'RECEIVE_CURRENT_TASK';
export const CREATE_TASK = 'CREATE_TASK';
export const COMPLETE_TASK = 'COMPLETE_TASK';
export const GET_ALL_TREES = 'GET_ALL_TREES';

export const ABORT_LABEL = 'ABORT_LABEL';
export const REQUEST_LABELS = 'REQUEST_LABELS';
export const RECEIVE_LABELS = 'RECEIVE_LABELS';
export const ADD_TASK_TO_LABEL = 'ADD_TASK_TO_LABEL';
export const GET_TASKS_BY_LABEL = 'GET_TASKS_BY_LABEL';
export const DELETE_LABEL = 'DELETE_LABEL';
