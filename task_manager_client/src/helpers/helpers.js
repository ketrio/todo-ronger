export const prettifySearchQuery = query => query.slice(1).split('_').join(' ');

export const renderCurrentDate = (defaultDate) => {
  const date = defaultDate || new Date();
  const month = date.getMonth();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  // eslint-disable-next-line max-len
  return `${date.getFullYear()}-${month - 10 < 0 ? 0 : ''}${month}-${date.getDate()}T${hours < 10 ? 0 : ''}${hours}:${minutes < 10 ? 0 : ''}${minutes}`;
};

export const isEqualArrays = (a, b) => {
  if (a.length !== b.length) return false;
  return a.every(e => b.includes(e));
};

// eslint-disable-next-line
const biggerArray = (a, b) => a > b ? a : b;

// eslint-disable-next-line
export const arraysDiff = (a, b) => a.reduce((accum, e) => b.includes(e) ? accum : [...accum, e], []);
