export const server = {
  hostname: 'http://localhost:8000/api',
  defaultUsername: 'users/vlad',
};

export const repeatVariants = {
  never: '',
  daily: 'Hourly',
  weeekly: 'Daily',
  monthly: 'Monthly',
  yearly: 'Yearly',
};
