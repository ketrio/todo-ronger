import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Badge from '@material-ui/core/Badge';
// import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
// import List from '@material-ui/core/List';
// import ListItem from '@material-ui/core/ListItem';
// import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
// import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';

import Notifications from '@material-ui/icons/Notifications';
// import AddIcon from '@material-ui/icons/Add';
// import MenuIcon from '@material-ui/icons/Menu';
// import LabelIcon from '@material-ui/icons/Label';
// import InboxIcon from '@material-ui/icons/AssignmentReturned';
// import ListIcon from '@material-ui/icons/List';
// import AllIcon from '@material-ui/icons/Assignment';
// import DoneIcon from '@material-ui/icons/AssignmentTurnedIn';

import styles from './styles';
import SearchBar from '../SearchBar/SearchBar';

class NavigationTop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  componentWillReceiveProps(newProps) {
    if (!newProps.collapsed) {
      this.setState({
        open: false,
      });
    }
  }

  toggleDrawer() {
    this.setState(prevState => ({
      open: !prevState.open,
    }));
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppBar
          position="fixed"
          className={classNames(classes.appBar)}
        >
          {/* {this.props.collapsed &&
            <IconButton
              color="inherit"
              aria-label="open drawer"
              className={classNames(classes.menuButton)}
              onClick={this.toggleDrawer}
            >
              <MenuIcon />
            </IconButton>
          } */}
          <Grid container spacing={0} className={classes.toolbar}>
            <Grid item xs={12} sm={4}>
              <Toolbar disableGutters>
                <Typography variant="title" color="inherit" noWrap>
                  TodoRonger
                </Typography>
              </Toolbar>
            </Grid>
            <Grid item xs={12} sm={8}>
              <Toolbar disableGutters>
                <SearchBar />
                {/* <IconButton
                  component={Link}
                  to="/create-task"
                  color="inherit"
                >
                  <AddIcon />
                </IconButton> */}
                <IconButton
                  component={Link}
                  to="/notifications"
                  color="inherit"
                >
                  <Badge badgeContent={3} color="secondary">
                    <Notifications />
                  </Badge>
                </IconButton>
              </Toolbar>
            </Grid>
          </Grid>
        </AppBar>
        {/* <Drawer
          variant={!this.props.collapsed ? 'permanent' : 'temporary'}
          classes={{
            paper: classNames(classes.drawerPaper),
          }}
          open={this.props.collapsed && this.state.open}
          onClose={this.toggleDrawer}
        >
          <div className={classes.toolbar} />
          <Divider />
          <List>
            <ListItem component={Link} to="/" button onClick={this.toggleDrawer}>
              <ListItemIcon>
                <AllIcon />
              </ListItemIcon>
              <ListItemText primary="All" />
            </ListItem>
            <ListItem component={Link} to="/?type=inbox" button onClick={this.toggleDrawer}>
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary="Inbox" />
            </ListItem>
            <ListItem component={Link} to="/?type=completed" button onClick={this.toggleDrawer}>
              <ListItemIcon>
                <DoneIcon />
              </ListItemIcon>
              <ListItemText primary="Completed" />
            </ListItem>
          </List>
        </Drawer> */}
      </React.Fragment>
    );
  }
}

NavigationTop.defaultProps = {
  //
};

NavigationTop.propTypes = {
  classes: PropTypes.object.isRequired,
  // collapsed: PropTypes.bool.isRequired,
};

export default withStyles(styles, { withTheme: true })(NavigationTop);
