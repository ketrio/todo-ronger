import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import style from './style';

const Spinner = ({ classes }) => (
  <div className={classes.root}>
    <CircularProgress className={classes.spinner} />
  </div>
);

Spinner.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(style)(Spinner);
