import React from 'react';
import { withStyles } from '@material-ui/core/styles';

import {
  Card,
  CardContent,
  CardActions,
  Button,
  Typography,
} from '@material-ui/core';

import style from './style';

const Notifications = (props) => {
  const { classes } = props;
  return [0, 1, 2].map(e => (
    <Card key={e} className={classes.card}>
      <CardContent>
        <Typography variant="headline">
          Task title #{e}
        </Typography>
        <Typography color="textSecondary">
          A long info bout task
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary">Go to task</Button>
        <Button size="small" color="primary">Dissmiss</Button>
      </CardActions>
    </Card>
  ));
};

export default withStyles(style)(Notifications);
