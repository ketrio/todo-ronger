import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Chip, Avatar } from '@material-ui/core';

import LabelIcon from '@material-ui/icons/Label';

import style from './style';

const Label = ({
  color,
  title,
  onDelete,
  classes,
}) => (
  <Chip
    avatar={<Avatar style={{ backgroundColor: color }}><LabelIcon nativeColor="#fff" /></Avatar>}
    label={title}
    className={classes.label}
    // clickable={!!onDelete}
    onDelete={onDelete}
  />
);

Label.defaultProps = {
  onDelete: null,
  color: '#3f50b5',
};

Label.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
  onDelete: PropTypes.func,
};

export default withStyles(style)(Label);
