import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Tabs,
  Tab,
  Typography,
  Divider,
  FormControl,
  Input,
  IconButton,
  InputAdornment,
} from '@material-ui/core';
import InboxIcon from '@material-ui/icons/AssignmentReturned';
import AllIcon from '@material-ui/icons/Assignment';
import DoneIcon from '@material-ui/icons/AssignmentTurnedIn';
import AddIcon from '@material-ui/icons/Add';
import LabelIcon from '@material-ui/icons/Label';
// import CloseIcon from '@material-ui/icons/Close';

import { getAllLabels, createLabel } from '../../actions/labels';

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabValue: 1,
      name: '',
    };
    props.getAllLabels();
    this.handleTabSwitch = this.handleTabSwitch.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTabSwitch(event, value) {
    this.setState({
      tabValue: value,
    });
  }

  handleInput(event) {
    this.setState({
      name: event.target.value,
    });
  }

  handleSubmit() {
    this.setState({
      name: '',
    });
    return this.state.tabValue !== 1
      ? undefined
      : this.props.createLabel(this.state.name);
  }

  render() {
    return (
      <React.Fragment>
        <List>
          <ListItem component={Link} to="/" button onClick={this.toggleDrawer}>
            <ListItemIcon>
              <AllIcon />
            </ListItemIcon>
            <ListItemText primary="All" />
          </ListItem>
          <ListItem component={Link} to="/?type=inbox" button onClick={this.toggleDrawer}>
            <ListItemIcon>
              <InboxIcon />
            </ListItemIcon>
            <ListItemText primary="Inbox" />
          </ListItem>
          <ListItem component={Link} to="/?type=completed" button onClick={this.toggleDrawer}>
            <ListItemIcon>
              <DoneIcon />
            </ListItemIcon>
            <ListItemText primary="Completed" />
          </ListItem>
        </List>
        <Tabs fullWidth value={this.state.tabValue} onChange={this.handleTabSwitch}>
          <Tab label="Projects" />
          <Tab label="Labels" />
        </Tabs>
        <List>
          {this.state.tabValue === 0 &&
            <Typography gutterBottom align="center" variant="caption">
              <br />
              No projects yet
            </Typography>
          }
          {this.state.tabValue === 1 &&
            this.props.labels.tree.map(label => (
              <ListItem key={label.id}>
                <ListItemIcon>
                  <LabelIcon />
                </ListItemIcon>
                <ListItemText primary={label.name} />
              </ListItem>
            ))
          }
        </List>
        <Divider />
        <br />
        <FormControl fullWidth>
          <Input
            id="create-category"
            value={this.state.name}
            onChange={this.handleInput}
            placeholder={`Create ${!this.state.tabValue ? 'project' : 'label'}`}
            disableUnderline
            startAdornment={
              <InputAdornment position="start">
                <IconButton
                  onClick={this.handleSubmit}
                  aria-label={`Add ${!this.state.tabValue ? 'project' : 'label'}`}
                >
                  <AddIcon />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </React.Fragment>
    );
  }
}

SideBar.propTypes = {
  labels: PropTypes.object.isRequired,
  getAllLabels: PropTypes.func.isRequired,
  createLabel: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    labels: state.labels,
  }),
  dispatch => ({
    getAllLabels: () => dispatch(getAllLabels()),
    createLabel: name => dispatch(createLabel(name)),
  }),
)(SideBar);
