import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Input,
  InputLabel,
  FormControl,
  Button,
  IconButton,
  List,
  Divider,
  InputAdornment,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

import TaskListItem from '../TaskListItem/TaskListItem';
import style from './style';
import RepeatSettings from '../RepeatSettings/RepeatSettings';
import LabelSelect from '../LabelSelect/LabelSelect';
import { renderCurrentDate, isEqualArrays, arraysDiff } from '../../helpers/helpers';
import { repeatVariants } from '../../helpers/config';
import { getTask, createTask, completeTask, updateTask } from '../../actions/tasks';
import { getAllLabels, addTaskToLabel } from '../../actions/labels';
import Spinner from '../Spinner/Spinner';

class TaskView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      labels_id: [],
      date: null,
      repeatSettings: {
        repeatVariant: repeatVariants.never,
        multiplier: 1,
        numberOfRepeats: 1,
        endOfRepeat: renderCurrentDate(),
      },
    };
    this.initialLabels = [];
    this.handlePanelClick = this.handlePanelClick.bind(this);
    this.setDate = this.setDate.bind(this);
    this.renderDatePicker = this.renderDatePicker.bind(this);
    this.handleTextInput = this.handleTextInput.bind(this);
    this.handleLabelsInput = this.handleLabelsInput.bind(this);
    this.handleRepeatInput = this.handleRepeatInput.bind(this);
    this.handleSave = this.handleSave.bind(this);

    props.getAllLabels();
    if (props.existing) {
      props.getTask(this.props.match.params.id);
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.current.name !== this.props.current.name) {
      this.initialLabels = newProps.current.labels
        ? newProps.current.labels.map(labelName => newProps.labels.find(label => label.name === labelName).id)
        : [];
      this.setState({
        name: newProps.current.name || '',
        description: newProps.current.description || '',
        date: newProps.current.date,
        labels_id: [...this.initialLabels],
      });
    }
  }

  setDate(date) {
    this.setState({ date });
  }

  handlePanelClick(index) {
    this.setState(prevState => ({
      expandedIndex: prevState.expandedIndex === index ? null : index,
    }));
  }

  handleTextInput(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleLabelsInput(event) {
    this.setState({
      labels_id: event.target.value,
    });
  }

  handleRepeatInput(repeatSettings) {
    this.setState({ repeatSettings });
  }

  handleSave(existing) {
    if (this.state.labels_id.length && !isEqualArrays(this.initialLabels, this.state.labels_id)) {
      arraysDiff(this.initialLabels, this.state.labels_id)
        .forEach(id => this.props.addTaskToLabel(id, this.props.current.id, 'DELETE'));
      arraysDiff(this.state.labels_id, this.initialLabels)
        .forEach(id => this.props.addTaskToLabel(id, this.props.current.id));
    }
    if (existing) this.props.updateTask({ id: this.props.current.id, ...this.state });
    else this.props.createTask(this.state);
  }

  renderDatePicker() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        {!this.state.date &&
          <Button
            className={classes.button}
            onClick={() => this.setDate(renderCurrentDate())}
          >
            Set date
          </Button>
        }
        {/* <Button onClick={() => this.setDate(this.state.date ? null : renderCurrentDate())}>Set date</Button> */}
        {this.state.date &&
          <FormControl>
            <InputLabel htmlFor="date">Date</InputLabel>
            <Input
              id="date"
              type="datetime-local"
              defaultValue={renderCurrentDate()}
              onChange={event => this.setDate(event.target.value)}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="add subtask"
                    onClick={() => this.setDate(null)}
                  >
                    <CloseIcon />
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
        }
      </React.Fragment>
    );
  }

  render() {
    const {
      classes,
      isFetching,
      current,
      existing,
    } = this.props;
    if (isFetching) {
      return (
        <Spinner />
      );
    }
    return (
      <React.Fragment>
        <FormControl fullWidth>
          <InputLabel
            htmlFor="name"
            required
          >
            Task name
          </InputLabel>
          <Input
            disableUnderline
            value={this.state.name}
            id="name"
            name="name"
            onChange={this.handleTextInput}
          />
        </FormControl>
        <FormControl fullWidth>
          <InputLabel
            htmlFor="description"
          >
            Description
          </InputLabel>
          <Input
            disableUnderline
            value={this.state.description}
            multiline
            rows={1}
            rowsMax={5}
            id="description"
            name="description"
            onChange={this.handleTextInput}
          />
        </FormControl>
        <LabelSelect labels={this.props.labels} value={this.state.labels_id} onChange={this.handleLabelsInput} />
        <List>
          {existing && current.subtasks && current.subtasks.map(task => (
            <TaskListItem
              key={task.id}
              labels={task.labels_id}
              completed={task.completed}
              onChange={() => this.props.completeTask(task.id)}
              task={{
                name: `${task.name}`,
                id: task.id,
                subtasks: task.subtasks,
              }}
              secondaryAction={
                <IconButton>
                  <CloseIcon />
                </IconButton>
              }
            />
          ))}
        </List>
        <FormControl fullWidth>
          <InputLabel htmlFor="subtask">Add subtask</InputLabel>
          <Input
            id="subtask"
            disableUnderline
            endAdornment={<IconButton aria-label="add subtask"><AddIcon /></IconButton>}
          />
        </FormControl>
        {this.renderDatePicker()}
        {this.state.date && <RepeatSettings value={this.state.repeatSettings} onChange={this.handleRepeatInput} />}
        {/* <Button onClick={() => console.log(this.state)}>Check</Button> */}
        <Divider />
        {/* <Button onClick={() => console.log(this.state)} variant="raised">Check</Button> */}
        <Button component={Link} to="/" className={classes.button} variant="raised">Back</Button>
        <Button
          component={Link}
          to="/"
          className={classes.button}
          variant="raised"
          color="primary"
          disabled={!this.state.name}
          onClick={this.handleSave}
        >
          Save
        </Button>
      </React.Fragment>
    );
  }
}

TaskView.defaultProps = {
  existing: false,
  match: {},
};

TaskView.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  existing: PropTypes.bool,
  isFetching: PropTypes.bool.isRequired,
  current: PropTypes.object.isRequired,
  labels: PropTypes.array.isRequired,

  getTask: PropTypes.func.isRequired,
  completeTask: PropTypes.func.isRequired,
  createTask: PropTypes.func.isRequired,
  updateTask: PropTypes.func.isRequired,
  getAllLabels: PropTypes.func.isRequired,
  addTaskToLabel: PropTypes.func.isRequired,
  match: PropTypes.object,
};

const Styled = withStyles(style)(TaskView);
export default connect(
  state => ({
    isFetching: state.tasks.isFetching,
    current: state.tasks.current,
    labels: state.labels.tree,
  }),
  dispatch => ({
    getTask: id => dispatch(getTask(id, true)),
    completeTask: id => dispatch(completeTask(id)),
    createTask: props => dispatch(createTask(props)),
    updateTask: ({ id, ...rest }) => dispatch(updateTask({ id, ...rest })),
    getAllLabels: () => dispatch(getAllLabels()),
    addTaskToLabel: (labelId, taskId, method) => dispatch(addTaskToLabel(labelId, taskId, method)),
  }),
)(Styled);
