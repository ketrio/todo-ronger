import React from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
// import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Drawer from '@material-ui/core/Drawer';
// import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';

import AddIcon from '@material-ui/icons/Add';

import styles from './styles';
import { prettifySearchQuery } from '../../helpers/helpers';
import { getAllTasks, completeTask, createTask } from '../../actions/tasks';
import TaskListItem from '../TaskListItem/TaskListItem';
import Spinner from '../Spinner/Spinner';

const filterByCompletion = query => (task) => {
  if (!query) return true;
  return query === 'completed' ? task.completed : !task.completed;
};

class TodoList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      detailsOpen: false,
      newTaskName: '',
    };
    this.toggleDrawer = this.toggleDrawer.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.props.getAllTasks();
  }

  toggleDrawer() {
    this.setState(prevState => ({
      detailsOpen: !prevState.detailsOpen,
    }));
  }

  handleInput(event) {
    this.setState({
      newTaskName: event.target.value,
    });
  }

  handleSubmit(key) {
    const newTaskName = this.state.newTaskName.trim();
    if (key === 'Enter' && newTaskName) {
      this.setState({
        newTaskName: '',
      });
      this.props.quickAddTask(this.state.newTaskName);
    }
  }

  render() {
    const {
      classes,
      search,
      location,
    } = this.props;

    return (
      <React.Fragment>
        {search &&
          <Typography
            variant="title"
          >
            {`Results for "${prettifySearchQuery(location.search)}":`}
          </Typography>
        }
        <List>
          {this.props.tree.filter(filterByCompletion(location.search.slice(6))).map(task => (
            <TaskListItem
              key={task.id}
              labels={task.labels}
              completed={task.completed}
              onChange={() => this.props.completeTask(task.id)}
              task={{
                name: `${task.name}`,
                id: task.id,
                subtasks: task.subtasks,
              }}
            />
          ))}
        </List>
        {this.props.isFetching && <Spinner />}
        {(location.search.slice(6) !== 'completed') &&
          <FormControl fullWidth>
            {/* <InputLabel htmlFor="add-task">Add new task</InputLabel> */}
            <Input
              id="add-task"
              value={this.state.newTaskName}
              onChange={this.handleInput}
              onKeyPress={event => this.handleSubmit(event.key)}
              disableUnderline
              placeholder="Add new task"
              startAdornment={
                <InputAdornment position="start">
                  <IconButton onClick={() => this.handleSubmit('Enter')} aria-label="Delete">
                    <AddIcon />
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
      }
        <Drawer
          anchor="right"
          classes={{
            paper: classes.drawerPaper,
          }}
          open={this.state.detailsOpen}
          onClose={this.toggleDrawer}
        >
          Some cute info
        </Drawer>
        {/* <Button
          component={Link}
          to="/create-task"
          variant="fab"
          className={classes.fab}
          color="primary"
        >
          <AddIcon />
        </Button> */}
      </React.Fragment>
    );
  }
}

TodoList.defaultProps = {
  search: false,
};

TodoList.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  tree: PropTypes.array.isRequired,
  getAllTasks: PropTypes.func.isRequired,
  completeTask: PropTypes.func.isRequired,
  quickAddTask: PropTypes.func.isRequired,
  search: PropTypes.bool,
  isFetching: PropTypes.bool.isRequired,
};

const Styled = withStyles(styles)(TodoList);
export default connect(
  state => state.tasks,
  dispatch => ({
    getAllTasks: () => dispatch(getAllTasks()),
    completeTask: id => dispatch(completeTask(id)),
    quickAddTask: (name) => {
      if (name) dispatch(createTask({ name }));
    },
  }),
)(Styled);
