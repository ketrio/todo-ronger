export default theme => ({
  root: {
    marginTop: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2,
  },
  formControl: {
    marginRight: theme.spacing.unit,
    marginBottom: theme.spacing.unit,
    minWidth: 200,
  },
});
