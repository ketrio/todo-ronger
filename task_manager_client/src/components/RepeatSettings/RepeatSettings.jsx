import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import {
  MenuItem,
  TextField,
  // Button,
} from '@material-ui/core';

import style from './style';
import { renderCurrentDate } from '../../helpers/helpers';
import { repeatVariants } from '../../helpers/config';

class RepeatSettings extends Component {
  constructor(props) {
    super(props);
    this.state = props.value || {
      repeatVariant: repeatVariants.never,
      multiplier: 1,
      numberOfRepeats: 1,
      endOfRepeat: renderCurrentDate(),
    };
    this.updateChoice = this.updateChoice.bind(this);
    this.setEndDate = this.setEndDate.bind(this);
  }

  setEndDate(event) {
    this.props.onChange({
      ...this.state,
      endOfRepeat: event.target.value,
    });
    this.setState({
      endOfRepeat: event.target.value,
    });
  }

  updateChoice(event) {
    this.props.onChange({
      ...this.state,
      [event.target.name]: event.target.value,
    });
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <TextField
          select
          id="repeatVariant"
          name="repeatVariant"
          label="Repeat"
          value={this.state.repeatVariant}
          onChange={this.updateChoice}
          className={classes.formControl}
        >
          {Object.keys(repeatVariants).map(key => (
            <MenuItem key={key} value={repeatVariants[key]}>
              {repeatVariants[key] === repeatVariants.never ? <em>Never</em> : repeatVariants[key]}
            </MenuItem>
          ))}
        </TextField>
        {this.state.repeatVariant !== '' &&
          <React.Fragment>
            <TextField
              id="multiplier"
              name="multiplier"
              label="Multiplier"
              value={this.state.multiplier}
              onChange={this.updateChoice}
              type="number"
              className={classes.formControl}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="numberOfRepeats"
              name="numberOfRepeats"
              label="Number of repeats"
              value={this.state.numberOfRepeats}
              onChange={this.updateChoice}
              type="number"
              className={classes.formControl}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              id="end-date"
              label="End date"
              defaultValue={this.state.endOfRepeat}
              type="datetime-local"
              onChange={this.setEndDate}
            />
          </React.Fragment>
        }
        {/* <Button onClick={() => console.log(this.state)}>Check</Button> */}
      </div>
    );
  }
}

RepeatSettings.defaultProps = {
  value: null,
  onChange: () => undefined,
};

RepeatSettings.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  value: PropTypes.shape({
    repeatVariant: PropTypes.string,
    multiplier: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    numberOfRepeats: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    endOfRepeat: PropTypes.string,
  }),
  onChange: PropTypes.func,
};

export default withStyles(style)(RepeatSettings);
