export default theme => ({
  paper: {
    padding: theme.spacing.unit / 2,
    margin: `0 ${theme.spacing.unit}px`,
    flex: 1,
  },
  formControl: {
    minWidth: 300,
  },
});
