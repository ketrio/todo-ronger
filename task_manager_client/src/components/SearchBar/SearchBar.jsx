import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';
import {
  Paper,
  FormControl,
  Input,
  InputAdornment,
  IconButton,
} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';

import style from './style';
import { prettifySearchQuery } from '../../helpers/helpers';

class CreateTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.location.pathname === '/search' ? prettifySearchQuery(props.location.search) : '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.location.pathname === '/search') {
      this.setState({
        value: prettifySearchQuery(nextProps.location.search),
      });
    } else if (this.state.value !== '') {
      this.setState({
        value: '',
      });
    }
  }

  handleChange(event) {
    this.setState({
      value: event.target.value,
    });
  }

  handleSubmit(key) {
    const value = this.state.value.trim();
    if (key === 'Enter' && value) {
      this.props.history.push({
        pathname: '/search',
        search: value.split(' ').join('_'),
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.paper}>
        <FormControl fullWidth className={classes.formControl}>
          <Input
            id="searchbar"
            placeholder="Search"
            disableUnderline
            value={this.state.value}
            onChange={this.handleChange}
            onKeyPress={event => this.handleSubmit(event.key)}
            startAdornment={
              <InputAdornment position="start">
                <IconButton
                  disabled={!this.state.value.trim()}
                  onClick={() => this.handleSubmit('Enter')}
                  aria-label="Toggle password visibility"
                >
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </Paper>
    );
  }
}

CreateTask.defaultProps = {
  //
};

CreateTask.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

const CreateTaskStyled = withStyles(style)(CreateTask);

export default withRouter(CreateTaskStyled);
