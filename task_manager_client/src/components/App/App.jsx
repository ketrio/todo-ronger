import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Switch, Route } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import NavigationTop from '../NavigationTop/NavigationTop';
import TodoList from '../TodoList/TodoList';
import TaskView from '../TaskView/TaskView';
import Notifications from '../Notifications/Notifications';
// import TaskView from '../TaskView/TaskView';

import styles from './styles';
import SideBar from '../SideBar/SideBar';

class App extends Component {
  constructor(props) {
    super(props);
    this.breakPoint = 700;
    this.state = {
      mobileView: window.innerWidth <= this.breakPoint,
    };
    this.onResize = this.onResize.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onResize);
  }

  onResize() {
    if (window.innerWidth > this.breakPoint && this.state.mobileView) {
      this.setState({
        mobileView: false,
      });
    } else if (window.innerWidth <= this.breakPoint && !this.state.mobileView) {
      this.setState({
        mobileView: true,
      });
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <CssBaseline>
        <div className={classes.root}>
          {/* <NavigationTop collapsed={this.state.mobileView} /> */}
          <NavigationTop />
          <Grid container spacing={16} className={classes.content}>
            <Grid item xs={12} sm={4}>
              <div className={classes.toolbar} />
              <div className={classes.sideBar}>
                <SideBar />
              </div>
            </Grid>
            <Grid item xs={12} sm={8}>
              <Paper className={classes.paper}>
                <div className={classes.toolbar} />
                <Switch>
                  <Route exact path="/" render={props => <TodoList {...props} />} />
                  <Route path="/create-task" render={() => <TaskView />} />
                  <Route path="/task/:id" render={props => <TaskView existing {...props} />} />
                  <Route path="/notifications" render={() => <Notifications />} />
                  <Route path="/search" render={props => <TodoList search {...props} />} />
                  <Route render={() => <Typography variant="title" align="center">Page not found</Typography>} />
                </Switch>
              </Paper>
            </Grid>
          </Grid>
        </div>
      </CssBaseline>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);
