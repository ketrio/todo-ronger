export default theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    position: 'relative',
    display: 'flex',
  },
  content: {
    backgroundColor: theme.palette.background.default,
    maxWidth: 1200,
    margin: '0 auto',
  },
  paper: {
    padding: theme.spacing.unit * 2,
    overflow: 'auto',
  },
  sideBar: {
    // position: 'fixed',
  },
  toolbar: theme.mixins.toolbar,
});
