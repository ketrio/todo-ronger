import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import {
  InputLabel,
  FormControl,
  Select,
  MenuItem,
  Checkbox,
  ListItemText,
  Input,
  Avatar,
  ListItemSecondaryAction,
} from '@material-ui/core';

import LabelIcon from '@material-ui/icons/Label';

import style from './style';
import Label from '../Label/Label';

class LabelSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onChange(event);
    this.setState({ value: event.target.value });
  }

  render() {
    const { classes } = this.props;
    return (
      <FormControl fullWidth className={classes.formControl}>
        <InputLabel htmlFor="labels">Labels</InputLabel>
        <Select
          multiple
          value={this.state.value}
          onChange={this.handleChange}
          input={<Input id="select-multiple" />}
          renderValue={labels => (
            <div className={classes.chips}>
              {labels.map((id) => {
                const label = this.props.labels.find(e => e.id === id);
                return label ? <Label key={label.id} title={label.name} /> : '';
              })}
            </div>
          )}
        >
          {this.props.labels.map(label => (
            <MenuItem
              key={label.id}
              value={label.id}
            >
              <Avatar style={{ backgroundColor: label.color || '#3f50b5' }}>
                <LabelIcon nativeColor="#fff" />
              </Avatar>
              <ListItemText primary={label.name} />
              <ListItemSecondaryAction>
                <Checkbox checked={this.state.value.indexOf(label.id) > -1} />
              </ListItemSecondaryAction>
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

LabelSelect.defaultProps = {
  onChange: () => undefined,
  value: [],
};

LabelSelect.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  onChange: PropTypes.func,
  value: PropTypes.array,
  labels: PropTypes.array.isRequired,
};

export default withStyles(style)(LabelSelect);
