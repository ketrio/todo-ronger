export default theme => ({
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    minWidth: 240,
    margin: `${theme.spacing.unit * 2}px 0`,
  },
});
