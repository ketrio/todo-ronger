export default () => ({
  root: {
    position: 'relative',
  },
  text: {
    marginLeft: 48,
  },
  secondaryActionClasses: {
    position: 'absolute',
    top: '50%',
    transform: 'translateY(-50%)',
    left: 4,
  },
  margin: {
    transition: 'height .2s',
  },
});
