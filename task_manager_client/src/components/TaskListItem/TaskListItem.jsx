import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox,
  Typography,
  Collapse,
  IconButton,
} from '@material-ui/core';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

import Label from '../Label/Label';

import styles from './styles';

class TaskListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      childrenOpen: false,
    };
    this.switchChildrenOpen = this.switchChildrenOpen.bind(this);
  }

  switchChildrenOpen() {
    this.setState(prevState => ({
      childrenOpen: !prevState.childrenOpen,
    }));
  }

  render() {
    const {
      task,
      onChange,
      disableLink,
      primaryAction,
      secondaryAction,
      classes,
      labels,
      completed,
    } = this.props;
    const { subtasks } = task;
    return (
      <React.Fragment>
        <li className={classes.root}>
          <ListItem
            ContainerComponent="div"
            component={!disableLink ? Link : 'div'}
            to={`/task/${task.id}`}
            button={!disableLink}
            divider
          >
            <ListItemText
              className={classes.text}
              disableTypography
              primary={<Typography variant="subheading">{task.name}</Typography>}
              secondary={
                labels && labels.map((label, i) => (
                  // eslint-disable-next-line
                  <Label key={`label-${i}`} title={label} />
                ))
              }
            />
            <ListItemSecondaryAction>
              {secondaryAction}
              {subtasks && subtasks.length > 0 &&
                <IconButton onClick={this.switchChildrenOpen}>
                  {this.state.childrenOpen ? <ExpandLess /> : <ExpandMore />}
                </IconButton>
              }
            </ListItemSecondaryAction>
          </ListItem>
          <div className={classes.secondaryActionClasses}>
            <Checkbox checked={completed} onChange={onChange} />
            {primaryAction}
          </div>
        </li>
        <Collapse in={this.state.childrenOpen} timeout="auto" unmountOnExit>
          {subtasks && subtasks.map(subtask => (
            <TaskListItem
              classes={this.props.classes}
              key={subtask.id}
              labels={subtask.labels_id}
              completed={subtask.completed}
              task={{
                name: `${subtask.name}`,
                id: subtask.id,
                subtasks: subtask.subtasks,
              }}
            />
          ))}
        </Collapse>
        <div className={classes.margin} style={{ height: this.state.childrenOpen ? 50 : 0 }} />
      </React.Fragment>
    );
  }
}

TaskListItem.defaultProps = {
  primaryAction: '',
  secondaryAction: '',
  onChange: () => undefined,
  disableLink: false,
  labels: [],
  completed: false,
};

TaskListItem.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  task: PropTypes.shape({
    id: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    name: PropTypes.string,
  }).isRequired,
  onChange: PropTypes.func,
  disableLink: PropTypes.bool,
  primaryAction: PropTypes.node,
  secondaryAction: PropTypes.node,
  labels: PropTypes.array,
  completed: PropTypes.bool,
};

export default withStyles(styles)(TaskListItem);
