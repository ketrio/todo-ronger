import * as types from '../actions/types';

const labels = (
  state = {
    isFetching: false,
    tree: [],
  },
  action,
) => ({
  [types.REQUEST_LABELS]: () => ({ ...state, isFetching: true }),
  [types.RECEIVE_LABELS]: () => ({
    ...state,
    isFetching: false,
    tree: action.label,
  }),
  [types.ABORT_TASK]: () => ({ ...state, isFetching: false }),
}[action.type] || (() => state))();

export default labels;
