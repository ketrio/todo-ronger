import * as types from '../actions/types';

const currentTask = (
  state = {
    isFetching: false,
    task: null,
  },
  action,
) => ({
  [types.REQUEST_CURRENT_TASK]: () => ({ ...state, isFetching: true }),
  [types.RECEIVE_CURRENT_TASK]: () => ({ ...state, isFetching: false, task: action.task }),
  [types.ABORT_TASK]: () => ({ ...state, isFetching: false }),
}[action.type] || (() => state))();

export default currentTask;
