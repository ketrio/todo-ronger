import * as types from '../actions/types';

const tasks = (
  state = {
    isFetching: false,
    tree: [],
    current: {},
  },
  action,
) => ({
  [types.REQUEST_TASK]: () => ({ ...state, isFetching: true }),
  [types.RECEIVE_TASK]: () => {
    // todo: enable pulling of only one task
    if (Array.isArray(action.task)) {
      return {
        ...state,
        current: {},
        isFetching: false,
        tree: action.task,
      };
    }
    return state;
  },
  [types.RECEIVE_CURRENT_TASK]: () => ({ ...state, isFetching: false, current: action.task }),
  [types.ABORT_TASK]: () => ({ ...state, isFetching: false }),
}[action.type] || (() => state))();

export default tasks;
