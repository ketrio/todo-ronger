from django.conf.urls import url, include
from django.views.generic import TemplateView

from tasks import views

urlpatterns = [
    url(r'users/(?P<username>\w+)/tasks/$', views.task_detail),
    url(r'users/(?P<username>\w+)/tasks/trees/$', views.get_task_trees),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/complete/$', views.complete_task),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/archive/$', views.archive_task),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/subtasks/$', views.get_subtasks),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/tree/$', views.get_task_tree),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/share/<str:collaborator>/$', views.share_task),
    url(r'users/(?P<username>\w+)/tasks/(?P<task_id>[0-9]+)/$', views.task_edit),

    url(r'users/(?P<username>\w+)/projects/$', views.project_detail),
    url(r'users/(?P<username>\w+)/projects/(?P<project_id>[0-9]+)/$', views.get_project),
    url(r'users/(?P<username>\w+)/projects/(?P<project_id>[0-9]+)/tasks/$', views.project_tasks_detail),

    url(r'users/(?P<username>\w+)/labels/$', views.label_detail),
    url(r'users/(?P<username>\w+)/labels/(?P<label_id>[0-9]+)/$', views.get_label),
    url(r'users/(?P<username>\w+)/labels/(?P<label_id>[0-9]+)/tasks/$', views.label_tasks_detail),

    url(r'users/(?P<username>\w+)/reminders/$', views.reminder_detail),
    url(r'users/(?P<username>\w+)/reminders/(?P<reminder_id>[0-9]+)/$', views.get_reminder),

    url(r'users/(?P<username>\w+)/rules/$', views.repeat_rule_detail),
]
