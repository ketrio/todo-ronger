from todo_lib.api import Api
from todo_lib.models import init_session
import os

DB_PATH = os.path.expanduser('~/.todoronger-web/')

if not os.path.exists(DB_PATH):
    os.makedirs(DB_PATH)

def init_service():
    session = init_session('sqlite', f"{DB_PATH}db.sqlite3")
    return Api(session)
