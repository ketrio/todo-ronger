from rest_framework.decorators import api_view
from rest_framework.response import Response
from dateutil.parser import parse

from tasks import init_service
from tasks.serializers import (
    TaskCreateSerializer,
    TaskShowSerializer,
    TaskEditSerializer,
    TaskTreeSerializer,
    ProjectSerializer,
    ProjectShowSerializer,
    ReminderSerializer,
    RepeatRuleSerializer
)

@api_view(['GET'])
def get_task_trees(request, username, format=None):
    api = init_service()
    task_trees = api.get_all_trees(
        user=username
    )
    serializer = TaskTreeSerializer(task_trees, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def complete_task(request, username, task_id, format=None):
    api = init_service()
    with api.save_after():
        api.complete_task(
            user=username,
            task_id=task_id
        )
    return Response()

@api_view(['POST', 'GET'])
def task_detail(request, username, format=None):
    api = init_service()
    if request.method == 'POST':
        serializer = TaskCreateSerializer(data=request.data)
        if serializer.is_valid():
            with api.save_after():
                task = api.create_task(user=username, **serializer.data)
            out = {
                'ID': task.id,
                'user': username,
                **serializer.data,
            }
            return Response(out)
        return Response(serializer.errors)
    elif request.method == 'GET':
        tasks = api.search_tasks(
            user=username,
            query=request.query_params.get('query', '')
        )
        serializer = TaskShowSerializer(tasks, many=True)
        return Response(serializer.data)

@api_view(['GET', 'PUT'])
def task_edit(request, username, task_id, format=None):
    api = init_service()
    if request.method == 'GET':
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        serializer = TaskShowSerializer(task)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = TaskEditSerializer(data=request.data)
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        if serializer.is_valid():
            with api.save_after():
                task.name = serializer.data.get('name') or task.name
                task.priority = serializer.data.get('priority') or task.priority
                task.description = serializer.data.get('description') or task.description
                task.date = serializer.data.get('date') or task.date
            return Response()
        return Response(serializer.errors)

@api_view(['GET'])
def get_subtasks(request, username, task_id, format=None):
    api = init_service()
    tasks = api.get_subtasks(
        user=username,
        task_id=task_id
    )
    serializer = TaskShowSerializer(tasks, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def get_task_tree(request, username, task_id, format=None):
    api = init_service()
    task = api.get_task(
        user=username,
        task_id=task_id
    )
    task_tree = api.get_subtask_tree(
        user=username,
        task=task
    )
    serializer = TaskTreeSerializer(task_tree)
    return Response(serializer.data)

@api_view(['GET'])
def get_all_trees(request, username, format=None):
    api = init_service()
    task_trees = api.get_all_trees(
        user=username
    )
    serializer = TaskTreeSerializer(task_trees, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def share_task(request, username, task_id, collaborator, format=None):
    api = init_service()
    with api.save_after():
        api.share_task(
            user=username,
            task_id=task_id,
            collaborator=collaborator
        )
    return Response()

@api_view(['POST'])
def archive_task(request, username, task_id, format=None):
    api = init_service()
    with api.save_after():
        task = api.archive_task(
            user=username,
            task_id=task_id
        )
    return Response()

@api_view(['GET', 'POST'])
def project_detail(request, username, format=None):
    api = init_service()

    if request.method == 'POST':
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            with api.save_after():
                project = api.create_project(
                    user=username,
                    **serializer.data
                )
            out = {
                'ID': project.id,
                'user': username,
                **serializer.data,
            }
            return Response(out)
        return Response(serializer.errors)
    if request.method == 'GET':
        projects = api.get_projects(
            user=username
        )
        serializer = ProjectShowSerializer(projects, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def get_project(request, username, project_id, format=None):
    api = init_service()
    project = api.get_project(
        user=username,
        project_id=project_id
    )
    serializer = ProjectShowSerializer(project)
    return Response(serializer.data)

@api_view(['GET', 'POST', 'DELETE'])
def project_tasks_detail(request, username, project_id, format=None):
    api = init_service()

    if request.method == 'GET':
        project = api.get_project(
            user=username,
            project_id=project_id
        )
        serializer = TaskShowSerializer(project.tasks, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        project = api.get_project(
            user=username,
            project_id=project_id
        )
        task_id = int(request.data.get('task_id'))
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        with api.save_after():
            project.tasks.append(task)
            task.project = project
        return Response()
    elif request.method == 'DELETE':
        project = api.get_project(
            user=username,
            project_id=project_id
        )
        task_id = int(request.data.get('task_id'))
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        with api.save_after():
            project.tasks.remove(task)
        return Response()

@api_view(['GET', 'POST'])
def label_detail(request, username, format=None):
    api = init_service()

    if request.method == 'POST':
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            with api.save_after():
                label = api.create_label(
                    user=username,
                    **serializer.data
                )
            out = {
                'ID': label.id,
                'user': username,
                **serializer.data,
            }
            return Response(out)
        return Response(serializer.errors)
    else:
        labels = api.get_labels(
            user=username
        )
        serializer = ProjectShowSerializer(labels, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def get_label(request, username, label_id, format=None):
    api = init_service()

    label = api.get_label(
        user=username,
        label_id=label_id
    )
    serializer = ProjectShowSerializer(label)
    return Response(serializer.data)

@api_view(['GET', 'POST', 'DELETE'])
def label_tasks_detail(request, username, label_id, format=None):
    api = init_service()

    if request.method == 'GET':
        label = api.get_label(
            user=username,
            label_id=label_id
        )
        serializer = TaskShowSerializer(label.tasks, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        label = api.get_label(
            user=username,
            label_id=label_id
        )
        task_id = int(request.data.get('task_id'))
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        with api.save_after():
            label.tasks.append(task)
        return Response()
    elif request.method == 'DELETE':
        label = api.get_label(
            user=username,
            label_id=label_id
        )
        task_id = int(request.data.get('task_id'))
        task = api.get_task(
            user=username,
            task_id=task_id
        )
        with api.save_after():
            label.tasks.remove(task)
        return Response()

@api_view(['GET', 'POST'])
def reminder_detail(request, username, format=None):
    api = init_service()

    if request.method == 'POST':
        date = parse(request.data.get('date'))
        task_id = int(request.data.get('task_id'))

        with api.save_after():
            api.create_reminder(
                user=username,
                task_id=task_id,
                date=date
            )

        return Response()
    else:
        reminders = api.get_reminders(
            user=username
        )
        serializer = ReminderSerializer(reminders, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def get_reminder(request, username, reminder_id, format=None):
    api = init_service()

    reminder = api.get_reminder(
        user=username,
        reminder_id=reminder_id
    )
    serializer = ReminderSerializer(reminder)
    return Response(serializer.data)

@api_view(['POST'])
def repeat_rule_detail(request, username, format=None):
    api = init_service()
    serializer = RepeatRuleSerializer(data=request.data)
    if serializer.is_valid():
        with api.save_after():
            rule = api.create_repeat_rule(user=username, **serializer.data)
        out = {
            'ID': rule.id,
            'user': username,
            **serializer.data,
        }
        return Response(out)
    return Response(serializer.errors)

