from rest_framework import serializers


class TaskCreateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256)
    priority = serializers.ChoiceField(
        choices=['low', 'normal', 'important', 'critical'],
        required=False
    )
    description = serializers.CharField(max_length=256, required=False)
    date = serializers.DateTimeField(format=None, required=False)
    parent_id = serializers.IntegerField(required=False)


class TaskEditSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256, required=False)
    priority = serializers.ChoiceField(
        choices=['low', 'normal', 'important', 'critical'],
        required=False
    )
    description = serializers.CharField(
        max_length=256, 
        required=False,
        allow_null=True,
        allow_blank=True
    )
    date = serializers.DateTimeField(
        format=None, 
        required=False, 
        allow_null=True
    )


class TaskShowSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    users = serializers.ListField(
        child=serializers.CharField(max_length=256),
    )
    archived = serializers.BooleanField()
    completed = serializers.BooleanField()
    name = serializers.CharField(max_length=256)
    project = serializers.CharField(max_length=256)
    priority = serializers.CharField(
        max_length=256,
        source='priority_name'
    )
    description = serializers.CharField(max_length=256)
    date = serializers.DateTimeField(format=None)
    parent_id = serializers.IntegerField()
    tree_root_id = serializers.IntegerField()
    created = serializers.DateTimeField(format=None)
    updated = serializers.DateTimeField(format=None)
    labels = serializers.ListField(
        child=serializers.CharField(max_length=256),
        source='labels_name'
    )
    repeat_rule_id = serializers.IntegerField()
    reminder = serializers.CharField()


class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class TaskTreeSerializer(TaskShowSerializer):
    subtasks = RecursiveField(many=True)

class ProjectSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256)


class ProjectShowSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=256)

class ReminderCreateSerializer(serializers.Serializer):
    task_id = serializers.IntegerField()
    date = serializers.DateTimeField()


class ReminderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    task_id = serializers.IntegerField()
    date = serializers.DateTimeField()


class RepeatRuleSerializer(serializers.Serializer):
    task_id = serializers.IntegerField()
    delta_type = serializers.ChoiceField(
        choices=[
            'year', 'month', 'day', 'hour'
        ]
    )
    delta_quantity = serializers.IntegerField(default=1)
    end_date = serializers.DateTimeField(required=False)
    count = serializers.IntegerField(required=False)
