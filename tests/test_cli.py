import os
import datetime
import subprocess
import unittest
import shutil
import tempfile

from todo_lib.api import Api
from todo_lib.models import init_session

APP_NAME = 'todoronger'

TEST_USER_NAME = 'test_user'
TEST_COLLABORATOR_NAME = 'test_collaborator'
TEST_TASK_NAME = 'test_task'
TEST_TASK_DATE = datetime.datetime(1999, 4, 15)
TEST_SUBTASK_NAME = 'test_subtask'
TEST_RULE_DELTA_QUANTITY = 3
TEST_RULE_COUNT = 4
TEST_PROJECT_NAME = 'test_project'
TEST_LABEL_NAME = 'test_label'

def call(cmd):
    with open(os.devnull, 'w') as devnull:
        subprocess.call(cmd, stdout=devnull, stderr=devnull)

class TestConsoleApp(unittest.TestCase):
    def setUp(self):
        self.directory = tempfile.mkdtemp()
        self.db_path = self.directory + 'test.sqlite3'
        self.session = init_session(
            'sqlite',
            self.db_path
        )
        self.api = Api(self.session)

    def test_task_create(self):
        cmd = [
            APP_NAME,
            'task',
            'create',
            TEST_TASK_NAME,
            '--database',
            self.db_path,
            '--user',
            TEST_USER_NAME
        ]
        call(cmd)
        tasks_len = len(
            self.api.get_tasks(
                user=TEST_USER_NAME
            )
        )
        self.assertEqual(tasks_len, 1)

    def test_add_subtask(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
            subtask = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_SUBTASK_NAME
            )
        cmd = [
            APP_NAME,
            'task',
            'attach',
            '1',
            '2',
            '--database',
            self.db_path,
            '--user',
            TEST_USER_NAME
        ]
        call(cmd)
        subtasks_len = len(
            self.api.get_subtasks(
                user=TEST_USER_NAME,
                task_id=1
            )
        )
        self.assertEqual(subtasks_len, 1)

    def test_task_complete(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
            subtask = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_SUBTASK_NAME
            )
        with self.api.save_after():
            self.api.add_subtask(
                user=TEST_USER_NAME,
                parent_task_id=task.id,
                subtask_id=subtask.id
            )
        cmd = [
            APP_NAME,
            'task',
            'complete',
            '1',
            '--database',
            self.db_path,
            '--user',
            TEST_USER_NAME
        ]
        call(cmd)
        self.assertTrue(subtask.completed)
        self.assertTrue(task.completed)

    def tearDown(self):
        shutil.rmtree(self.directory)
