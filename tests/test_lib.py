import os
import unittest
import shutil
import tempfile
import datetime

from todo_lib.api import Api
from todo_lib.models import (
    init_session,
    DeltaType
)


TEST_USER_NAME = 'test_user'
TEST_COLLABORATOR_NAME = 'test_collaborator'
TEST_TASK_NAME = 'test_task'
TEST_TASK_DATE = datetime.datetime(1999, 4, 15)
TEST_SUBTASK_NAME = 'test_subtask'
TEST_RULE_DELTA_TYPE = DeltaType.MONTH
TEST_RULE_DELTA_QUANTITY = 3
TEST_RULE_COUNT = 4
TEST_PROJECT_NAME = 'test_project'
TEST_LABEL_NAME = 'test_label'


class TestApi(unittest.TestCase):
    def setUp(self):
        self.directory = tempfile.mkdtemp()
        self.session = init_session(
            'sqlite',
            self.directory + 'test.sqlite3'
        )
        self.api = Api(self.session)

    def test_task_create(self):
        self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME
        )
        tasks_len = len(
            self.api.get_tasks(user=TEST_USER_NAME)
        )
        self.assertEqual(tasks_len, 1)

    def test_share_task(self):
        with self.api.save_after():
            self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
        tasks_len = len(
            self.api.get_tasks(user=TEST_COLLABORATOR_NAME)
        )
        self.assertEqual(tasks_len, 0)

        self.api.share_task(
            user=TEST_USER_NAME,
            task_id=1,
            collaborator=TEST_COLLABORATOR_NAME
        )
        tasks_len = len(
            self.api.get_tasks(user=TEST_COLLABORATOR_NAME)
        )
        self.assertEqual(tasks_len, 1)

    def test_add_subtask(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
            subtask = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_SUBTASK_NAME
            )
        subtasks_len = len(
            self.api.get_subtasks(
                user=TEST_USER_NAME,
                task_id=task.id
            )
        )
        self.assertEqual(subtasks_len, 0)

        self.api.add_subtask(
            user=TEST_USER_NAME,
            parent_task_id=task.id,
            subtask_id=subtask.id
        )
        subtasks_len = len(
            self.api.get_subtasks(
                user=TEST_USER_NAME,
                task_id=task.id
            )
        )
        self.assertEqual(subtasks_len, 1)

    def test_task_search(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
        tasks_len = len(
            self.api.search_tasks(
                user=TEST_USER_NAME,
                query=TEST_TASK_NAME[:3]
            )
        )
        self.assertEqual(tasks_len, 1)

    def test_task_complete(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
            subtask = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_SUBTASK_NAME
            )
        self.api.add_subtask(
            user=TEST_USER_NAME,
            parent_task_id=task.id,
            subtask_id=subtask.id
        )
        self.api.complete_task(
            user=TEST_USER_NAME,
            task_id=task.id
        )

        self.assertTrue(task.completed)
        self.assertTrue(subtask.completed)

    def test_task_archive(self):
        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
            subtask = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_SUBTASK_NAME
            )
        self.api.add_subtask(
            user=TEST_USER_NAME,
            parent_task_id=task.id,
            subtask_id=subtask.id
        )
        self.api.archive_task(
            user=TEST_USER_NAME,
            task_id=task.id
        )

        self.assertTrue(task.archived)
        self.assertTrue(subtask.archived)


    def test_create_project(self):
        projects_len = len(
            self.api.get_projects(user=TEST_USER_NAME)
        )
        self.assertEqual(projects_len, 0)

        with self.api.save_after():
            self.api.create_project(
                user=TEST_USER_NAME,
                name=TEST_PROJECT_NAME
            )
        projects_len = len(
            self.api.get_projects(user=TEST_USER_NAME)
        )
        self.assertEqual(projects_len, 1)

    def test_create_label(self):
        labels_len = len(
            self.api.get_labels(user=TEST_USER_NAME)
        )
        self.assertEqual(labels_len, 0)

        with self.api.save_after():
            self.api.create_label(
                user=TEST_USER_NAME,
                name=TEST_LABEL_NAME
            )
        labels_len = len(
            self.api.get_labels(user=TEST_USER_NAME)
        )
        self.assertEqual(labels_len, 1)

    def test_create_rule(self):
        tasks_len = len(
            self.api.get_tasks(
                user=TEST_USER_NAME
            )
        )
        self.assertEqual(tasks_len, 0)

        rules_len = len(
            self.api.get_repeat_rules(
                user=TEST_USER_NAME
            )
        )
        self.assertEqual(rules_len, 0)

        with self.api.save_after():
            task = self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME,
                date=TEST_TASK_DATE
            )

        self.api.create_repeat_rule(
            user=TEST_USER_NAME,
            task_id=task.id,
            delta_type=TEST_RULE_DELTA_TYPE,
            delta_quantity=TEST_RULE_DELTA_QUANTITY,
            count=TEST_RULE_COUNT
        )
        self.api.apply_repeat_rules(
            self.api.check_repeat_rules(
                user=TEST_USER_NAME
            )
        )

        tasks_len = len(
            self.api.get_tasks(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
        )

        self.assertEqual(
            tasks_len,
            TEST_RULE_COUNT + 1
        )

    def test_api_abort(self):
        self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME
        )
        self.api.abort()
        tasks_len = len(
            self.api.get_tasks(user=TEST_USER_NAME)
        )
        self.assertEqual(tasks_len, 0)

    def test_api_save(self):
        self.api.create_task(
            user=TEST_USER_NAME,
            name=TEST_TASK_NAME
        )
        self.api.save()
        tasks_len = len(
            self.api.get_tasks(user=TEST_USER_NAME)
        )
        self.assertEqual(tasks_len, 1)

        self.api.abort()
        tasks_len = len(
            self.api.get_tasks(user=TEST_USER_NAME)
        )
        self.assertEqual(tasks_len, 1)

    def test_api_save_context_manager(self):
        with self.api.save_after():
            self.api.create_task(
                user=TEST_USER_NAME,
                name=TEST_TASK_NAME
            )
        tasks_len = len(
            self.api.get_tasks(user=TEST_USER_NAME)
        )
        self.assertEqual(tasks_len, 1)

    def tearDown(self):
        shutil.rmtree(self.directory)
