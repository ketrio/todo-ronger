from setuptools import (
    setup,
    find_packages
 )

setup(
    name="todoronger",
    version="0.0.1",
    description="Simple todo app for your productivity",
    author="Vlad Gutkovsky",
    author_email="vladgutkovskii@gmail.com",
    url="https://bitbucket.org/ketrio/todo-ronger",
    packages=find_packages(exclude=["tests"]),
    entry_points={
        "console_scripts": [
            "todoronger=todo_cli.main:main",
        ],
    },
    test_suite='tests',
    install_requires=["sqlalchemy", "python-dateutil"]
)
