from getpass import getuser
from os.path import expanduser
import sys
import warnings

from todo_lib.api import Api

from todo_cli.config import (
    DB_TYPE,
    DB_PATH,
    WARNING_FILTER
)
from todo_cli.parser import parse_args
from todo_cli.commands import command_handle
from todo_cli.logging import (
    init_lib_logging,
    init_cli_logging
)
from todo_lib.models import init_session


def main():
    init_cli_logging()
    init_lib_logging()

    args = parse_args()
    username = args.user or getuser()

    warnings.filterwarnings(WARNING_FILTER)

    if args.database is not None:
        session = init_session(DB_TYPE, expanduser(args.database))
    else:
        session = init_session(DB_TYPE, expanduser(DB_PATH))

    api = Api(session)
    api.apply_repeat_rules(api.check_repeat_rules(username))

    command_handle(api, username, args)
