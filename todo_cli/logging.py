from logging import (
    getLevelName,
    Formatter,
    FileHandler,
    getLogger
)
from os.path import expanduser
from functools import wraps

from todo_lib.logging import get_logger
from todo_cli.config import (
    LIB_LOGGER_FILE,
    LIB_LOGGER_LEVEL,
    LIB_LOGGER_FORMAT,
    LIB_LOGGER_DATE_FORMAT,
    CLI_LOGGER_FILE,
    CLI_LOGGER_LEVEL,
    CLI_LOGGER_FORMAT,
    CLI_LOGGER_DATE_FORMAT
)

CLI_LOGGER_NAME = "todo_cli"

def init_lib_logging():
    logger = get_logger()

    logger_level = getLevelName(LIB_LOGGER_LEVEL)
    logger_handler = FileHandler(expanduser(LIB_LOGGER_FILE))
    logger_formatter = Formatter(
        LIB_LOGGER_FORMAT,
        LIB_LOGGER_DATE_FORMAT
    )

    logger_handler.setFormatter(logger_formatter)
    logger.setLevel(logger_level)
    logger.addHandler(logger_handler)

    return logger

def init_cli_logging():
    logger = getLogger(CLI_LOGGER_NAME)

    logger_level = getLevelName(CLI_LOGGER_LEVEL)
    logger_handler = FileHandler(expanduser(CLI_LOGGER_FILE))
    logger_formatter = Formatter(
        CLI_LOGGER_FORMAT,
        CLI_LOGGER_DATE_FORMAT
    )

    logger_handler.setFormatter(logger_formatter)
    logger.setLevel(logger_level)
    logger.addHandler(logger_handler)

    return logger

def get_lib_logger():
    return get_logger

def get_cli_logger():
    return getLogger(CLI_LOGGER_NAME)

def log_call_and_exception(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        LOGGER = getLogger(CLI_LOGGER_NAME)
        LOGGER.info(f"function call: {f.__name__}")
        LOGGER.debug(f"args: {args}")
        LOGGER.debug(f"kwargs: {kwargs}")
        try:
            res = f(*args, **kwargs)
            LOGGER.debug(f"result: {res!r}")
            return res
        except Exception as e:
            LOGGER.error(e)
            LOGGER.exception(e)
            raise
    return wrapper
