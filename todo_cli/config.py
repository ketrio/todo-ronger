DB_TYPE = "sqlite"
DB_PATH =  "~/.todoronger/db.sqlite3"

LIB_LOGGER_FILE = "~/.todoronger/lib_log"
LIB_LOGGER_LEVEL = "DEBUG"
LIB_LOGGER_FORMAT = "%(asctime)s %(name)-12s %(levelname)-8s %(message)s"
LIB_LOGGER_DATE_FORMAT = "%m-%d %H:%M"

CLI_LOGGER_FILE = "~/.todoronger/cli_log"
CLI_LOGGER_LEVEL = "DEBUG"
CLI_LOGGER_FORMAT = "%(asctime)s %(name)-12s %(levelname)-8s %(message)s"
CLI_LOGGER_DATE_FORMAT = "%m-%d %H:%M"

WARNING_FILTER = "error"
