import os
from os.path import (
    expanduser,
    dirname
)

from todo_cli.config import DB_PATH

db_path = dirname(expanduser(DB_PATH))

if not os.path.exists(db_path):
    os.makedirs(db_path)
