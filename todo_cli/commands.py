import textwrap
import sys
from datetime import datetime
from functools import wraps

from todo_lib.models import (
    RuleEndCondition,
    Priority
)
from todo_lib.exceptions import (
    LibraryError,
    LibraryWarning
)
from todo_cli.logging import log_call_and_exception


def handle_command_error(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except LibraryError as e:
            print(e, file=sys.stderr)
            sys.exit(1)
        except LibraryWarning as w:
            print(w, file=sys.stderr)
            sys.exit(1)
        except ValueError as e:
            print(e, file=sys.stderr)
            sys.exit(1)
        except Exception as e:
            print(
                "Internal Error. An uncaught exception was raised."
                " See log for more details.",
                file=sys.stderr
            )
            sys.exit(1)
    return wrapper


@log_call_and_exception
def print_task(task, indent=4, level=0):
    """Print task hierarchy with indent

    Args:
        task (Task):
        indent (int, optional): Defaults to 4. Count of indentional spaces
        level (int, optional): Defaults to 0. Hierarchy level
    """

    print(textwrap.indent(str(task), " " + " " * indent * level))
    for child in task.subtasks:
        print()
        print_task(child, indent, level + 1)


@log_call_and_exception
def task_handle(api, user, namespace):
    """Task command handler

    Args:
        api (Api): Api instance
        namespace (Namespace): Command namespace
    """

    if namespace.method == "create":
        with api.save_after():
            task = api.create_task(
                user=user,
                priority=namespace.priority,
                name=namespace.name,
                description=namespace.description,
                date=namespace.date,
                parent_id=namespace.parent
            )
        print(f"Task is successfully created. [{task.id}]")
    elif namespace.method == "list":
        if namespace.archived and namespace.completed:
            tasks = api.get_all_trees(user=user)
        elif namespace.archived:
            tasks = api.get_all_trees(user=user, completed=False)
        elif namespace.completed:
            tasks = api.get_all_trees(user=user, archived=False)
        else:
            tasks = api.get_all_trees(
                user=user,
                archived=False,
                completed=False
            )
        if not tasks:
            print("No tasks.")
        for task in tasks:
            print_task(task)
            print()
    elif namespace.method == "complete":
        with api.save_after():
            api.complete_task(
                user=user,
                task_id=namespace.task_id
            )
        print("Task is completed.")
    elif namespace.method == "archive":
        with api.save_after():
            api.archive_task(
                user=user,
                task_id=namespace.task_id
            )
        print("Task is archived.")
    elif namespace.method == "show":
        task = api.get_task(user=user, task_id=namespace.task_id)
        print(task)
    elif namespace.method == "edit":
        task = api.get_task(user=user, task_id=namespace.task_id)
        with api.save_after():
            if namespace.name:
                task.name = namespace.name
            if namespace.description:
                task.description = namespace.description
            if namespace.date:
                task.date = namespace.date
            if namespace.priority:
                task.priority = Priority[namespace.priority.upper()]
        print("Edited task:")
        print()
        print(task)
    elif namespace.method == "attach":
        for subtask_id in namespace.subtasks:
            with api.save_after():
                api.add_subtask(
                    user=user,
                    parent_task_id=namespace.task_id,
                    subtask_id=subtask_id
                )
        print("Tasks is attached.")
    elif namespace.method == "detach":
        with api.save_after():
            task = api.get_task(user=user, task_id=namespace.task_id)
            task.parent_id = None
        print("Task is detached.")
    elif namespace.method == "share":
        with api.save_after():
            api.share_task(
                user=user,
                task_id=namespace.task_id,
                collaborator=namespace.collaborator
            )
        print(f"Task is shared with {namespace.collaborator}.")
    elif namespace.method == "search":
        tasks = api.search_tasks(user=user, query=namespace.query)
        if not tasks:
            print("No tasks are found.")
            return
        for task in tasks:
            print_task(task)
            print()


@log_call_and_exception
def project_handle(api, user, namespace):
    """Project command handler

    Args:
        api (Api): Api instance
        namespace (Namespace): Command namespace
    """

    if namespace.method == "create":
        with api.save_after():
            project = api.create_project(user=user, name=namespace.name)
        print(f"Project created successfully. [{project.id}]")
    elif namespace.method == "list":
        projects = api.get_projects(user=user)
        if not projects:
            print("No projects")
        else:
            for project in projects:
                print(project)
                print()
    elif namespace.method == "show":
        project = api.get_project(
            user=user,
            project_id=namespace.project_id
        )
        print(project)
        print()
        for task in project.tasks:
            print(task)
            print()
    elif namespace.method == "edit":
        with api.save_after():
            project = api.get_project(
                user=user,
                project_id=namespace.project_id
            )
            if namespace.name:
                project.name = namespace.name
        print("Project edited.")
    elif namespace.method == "add":
        with api.save_after():
            project = api.get_project(
                user=user,
                project_id=namespace.project_id
            )
            tasks = []
            for task_id in namespace.task_ids:
                task = api.get_task(user=user, task_id=task_id)
                tasks.append(task)
            project.tasks.extend(tasks)
            print("Tasks added to the project.")
            print()
            print(project)
    elif namespace.method == "remove":
        with api.save_after():
            project = api.get_project(
                user=user,
                project_id=namespace.project_id
            )
            tasks = []
            for task_id in namespace.task_ids:
                task = api.get_task(user=user, task_id=task_id)
                if task in project.tasks:
                    project.tasks.remove(task)
                else:
                    raise ValueError(
                        f"Project does not contain task with given id ({task_id})"
                    )
            print("Tasks are removed from the project.")
    elif namespace.method == "delete":
        project = api.get_project(
            user=user,
            project_id=namespace.project_id
        )
        with api.save_after():
            api.delete(project)
        print("Project is deleted.")


@log_call_and_exception
def label_handle(api, user, namespace):
    """Label command handler

    Args:
        api (Api): Api instance
        namespace (Namespace): Command namespace
    """

    if namespace.method == "create":
        with api.save_after():
            label = api.create_label(user=user, name=namespace.name)
        print(f"label created successfully. [{label.id}]")
    elif namespace.method == "list":
        labels = api.get_labels(user=user)
        if not labels:
            print("No labels.")
        for label in labels:
            print(f"[{label.id}] {label.name}")
            print(f"tasks: {len(label.tasks)}")
            print()
    elif namespace.method == "show":
        label = api.get_label(
            user=user,
            label_id=namespace.label_id
        )
        print(label.name)
        print()
        for task in label.tasks:
            print(task)
            print()
    elif namespace.method == "edit":
        with api.save_after():
            label = api.get_label(
                user=user,
                label_id=namespace.label_id
            )
            if namespace.name:
                label.name = namespace.name
        print("Label is successfully edited.")
    elif namespace.method == "mark":
        with api.save_after():
            label = api.get_label(
                user=user,
                label_id=namespace.label_id
            )
            tasks = []
            for task_id in namespace.task_ids:
                task = api.get_task(user=user, task_id=task_id)
                tasks.append(task)
            label.tasks.extend(tasks)
        print("Tasks are marked.")
    elif namespace.method == "unmark":
        with api.save_after():
            label = api.get_label(
                user=user,
                label_id=namespace.label_id
            )
            tasks = []
            for task_id in namespace.task_ids:
                task = api.get_task(user=user, task_id=task_id)
                if task in label.tasks:
                    label.tasks.remove(task)
                else:
                    raise ValueError(
                        f"Project does not contain task with given id ({task_id})"
                    )
        print("Tasks are unmarked.")
    elif namespace.method == "delete":
        label = api.get_label(
            user=user,
            label_id=namespace.label_id
        )
        if label.predefined:
            print("Label is predefined")
        else:
            api.delete(label)
            print("Label successfully removed")


@log_call_and_exception
def reminder_handle(api, user, namespace):
    if namespace.method == "create":
        with api.save_after():
            reminder = api.create_reminder(
                user=user,
                task_id=namespace.task_id,
                date=namespace.date
            )
        print(f"Reminder is created. [{reminder.id}]")
    elif namespace.method == "remove":
        reminder = api.get_reminder(
            user=user,
            reminder_id=namespace.reminder_id
        )
        api.delete(reminder)
    elif namespace.method == "list":
        reminders = api.get_reminders(user=user)
        if reminders:
            for reminder in reminders:
                print(reminder)
                print()
        else:
            print("No reminders")


@log_call_and_exception
def rule_handle(api, user, namespace):
    if namespace.method == "create":
        with api.save_after():
            rule = api.create_repeat_rule(
                user=user,
                task_id=namespace.task_id,
                delta_type=namespace.every,
                delta_quantity=namespace.quantity,
                end_date=namespace.end_date,
                count=namespace.end_count
            )
        print(f"Rule successfully created [{rule.id}]")
    if namespace.method == "remove":
        with api.save_after():
            rule = api.get_repeat_rule(
                user=user,
                rule_id=namespace.rule_id
            )
            api.delete(rule)
            print("Rule successfully removed")


@handle_command_error
@log_call_and_exception
def command_handle(api, user, args):
    if args.subject == "task":
        task_handle(api, user, args)
    elif args.subject == "project":
        project_handle(api, user, args)
    elif args.subject == "label":
        label_handle(api, user, args)
    elif args.subject == "reminder":
        reminder_handle(api, user, args)
    elif args.subject == "rule":
        rule_handle(api, user, args)
