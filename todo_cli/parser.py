import argparse
import sys
from datetime import datetime
from dateutil.parser import parse

from todo_lib.models import DeltaType
from todo_cli.logging import (
    get_cli_logger,
    log_call_and_exception
)

class DefaultHelpParser(argparse.ArgumentParser):
    @log_call_and_exception
    def error(self, message):
        print(f"error: {message}", file=sys.stderr)
        self.print_help()
        sys.exit(2)

@log_call_and_exception
def valid_datetime(text):
    """Parse string

    Formats:
        20.04.1999
        20.04.1999 15:43

    Args:
        text (str): date string

    Raises:
        ValueError: not supported format

    Returns:
        datetime: processed object
    """
    try:
        return parse(text)
    except ValueError as e:
        raise argparse.ArgumentTypeError("Not valid date format") from e


@log_call_and_exception
def valid_delta_type(text):
    try:
        return DeltaType[text.upper()]
    except KeyError as e:
        raise argparse.ArgumentTypeError("Not valid delta type format") from e

@log_call_and_exception
def init_task_parser(parent_parser, subparsers):
    task_parser = subparsers.add_parser(
        "task",
        help="Task commands"
    )
    task_subparsers = task_parser.add_subparsers(dest="method")
    task_subparsers.required = True

    task_create = task_subparsers.add_parser(
        "create",
        parents=[parent_parser],
        help="Create task"
    )
    task_create.add_argument(
        "name",
        help="Name of task"
    )
    task_create.add_argument(
        "--description",
        help="Description of task"
    )
    task_create.add_argument(
        "--date",
        type=valid_datetime,
        help="Date for task to be completed"
    )
    task_create.add_argument(
        "--parent",
        "-p",
        type=int,
        help="Parent task ID"
    )
    task_create.add_argument(
        "--priority",
        action="store",
        choices=["low", "normal", "important", "critical"],
        default="low",
        help="Task priority"
    )

    task_list = task_subparsers.add_parser(
        "list",
        parents=[parent_parser],
        help="List all tasks"
    )
    task_list.add_argument(
        "--completed",
        action="store_true",
        help="Show completed tasks"
    )
    task_list.add_argument(
        "--archived",
        action="store_true",
        help="Show archived tasks"
    )

    task_show = task_subparsers.add_parser(
        "show",
        parents=[parent_parser],
        help="Show specific task"
    )
    task_show.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )

    task_complete = task_subparsers.add_parser(
        "complete",
        parents=[parent_parser],
        help="Complete task"
    )
    task_complete.add_argument(
        "task_id",
        type=int,
        help="Task id"
    )

    task_edit = task_subparsers.add_parser(
        "edit",
        parents=[parent_parser],
        help="Edit specific task"
    )
    task_edit.add_argument(
        "task_id",
        type=int,
        help="ID of task to be edited"
    )
    task_edit.add_argument(
        "--name",
        help="Task name"
    )
    task_edit.add_argument(
        "--description",
        help="Task description"
    )
    task_edit.add_argument(
        "--date",
        type=valid_datetime,
        help="Task date"
    )
    task_edit.add_argument(
        "--priority",
        help="Task priority",
        choices=["low", "normal", "important", "critical"]
    )

    task_attach = task_subparsers.add_parser(
        "attach",
        parents=[parent_parser],
        help="Attach task to a task tree"
    )
    task_attach.add_argument(
        "task_id",
        type=int,
        help="Parent task"
    )
    task_attach.add_argument(
        "subtasks",
        type=int,
        nargs="+",
        help="Children tasks"
    )

    task_detach = task_subparsers.add_parser(
        "detach",
        parents=[parent_parser],
        help="Remove task from a task tree"
    )
    task_detach.add_argument(
        "task_id",
        type=int,
        help="ID of task to be removed"
    )

    task_share = task_subparsers.add_parser(
        "share",
        parents=[parent_parser],
        help="Share task with other user"
    )
    task_share.add_argument(
        "task_id",
        type=int,
        help="ID of task to be shared"
    )
    task_share.add_argument(
        "collaborator",
        help="Collaborator\"s username"
    )

    task_archive = task_subparsers.add_parser(
        "archive",
        parents=[parent_parser],
        help="archive task"
    )
    task_archive.add_argument(
        "task_id",
        type=int,
        help="Task ID"
    )

    task_search = task_subparsers.add_parser(
        "search",
        parents=[parent_parser],
        help='Search tasks'
    )
    task_search.add_argument(
        "query",
        help='Search query'
    )

    return task_parser


@log_call_and_exception
def init_project_parser(parent_parser, subparsers):
    project_parser = subparsers.add_parser(
        "project",
        help="Project commands"
    )
    project_subparsers = project_parser.add_subparsers(dest="method")
    project_subparsers.required = True

    project_create = project_subparsers.add_parser(
        "create",
        parents=[parent_parser],
        help="Create task"
    )
    project_create.add_argument(
        "name",
        help="Project name"
    )

    project_subparsers.add_parser(
        "list",
        parents=[parent_parser],
        help="List all projects"
    )

    project_show = project_subparsers.add_parser(
        "show",
        parents=[parent_parser],
        help="Show specific task"
    )
    project_show.add_argument(
        "project_id",
        type=int,
        help="Project id"
    )

    project_edit = project_subparsers.add_parser(
        "edit",
        parents=[parent_parser],
        help="Edit project"
    )
    project_edit.add_argument(
        "project_id",
        type=int,
        help="Project id"
    )
    project_edit.add_argument(
        "--name",
        help="Project name"
    )

    project_add = project_subparsers.add_parser(
        "add",
        parents=[parent_parser],
        help="Add task to project"
    )
    project_add.add_argument(
        "project_id",
        type=int,
        help="Project id"
    )
    project_add.add_argument(
        "task_ids",
        nargs="*",
        type=int,
        help="ID of tasks to be added"
    )

    project_remove = project_subparsers.add_parser(
        "remove",
        parents=[parent_parser],
        help="Remove task from the project"
    )
    project_remove.add_argument(
        "project_id",
        type=int,
        help="Project id"
    )
    project_remove.add_argument(
        "task_ids",
        nargs="*",
        type=int,
        help="ID of tasks to be removed"
    )

    project_delete = project_subparsers.add_parser(
        "delete",
        parents=[parent_parser],
        help="Delete project"
    )
    project_delete.add_argument(
        "project_id",
        type=int,
        help="Project ID"
    )

    return project_parser


@log_call_and_exception
def init_label_parser(parent_parser, subparsers):
    label_parser = subparsers.add_parser(
        "label",
        help="Label commands"
    )
    label_subparsers = label_parser.add_subparsers(dest="method")
    label_subparsers.required = True

    label_create = label_subparsers.add_parser(
        "create",
        parents=[parent_parser],
        help="Create label"
    )
    label_create.add_argument(
        "name",
        help="Label name"
    )

    label_subparsers.add_parser(
        "list",
        parents=[parent_parser],
        help="List all labels"
    )

    label_show = label_subparsers.add_parser(
        "show",
        parents=[parent_parser],
        help="Show specific label"
    )
    label_show.add_argument(
        "label_id",
        type=int,
        help="Label ID"
    )

    label_edit = label_subparsers.add_parser(
        "edit",
        parents=[parent_parser],
        help="Edit label name"
    )
    label_edit.add_argument(
        "label_id",
        type=int,
        help="Label id"
    )
    label_edit.add_argument(
        "--name",
        help="Label name"
    )

    label_mark = label_subparsers.add_parser(
        "mark",
        parents=[parent_parser],
        help="Mark tasks with label"
    )
    label_mark.add_argument("label_id", type=int, help="Label ID")
    label_mark.add_argument(
        "task_ids",
        nargs="*",
        type=int,
        help="ID of tasks to be marked"
    )

    label_unmark = label_subparsers.add_parser(
        "unmark",
        parents=[parent_parser],
        help="Unmark task from labels"
    )
    label_unmark.add_argument(
        "label_id",
        type=int,
        help="Label ID"
    )
    label_unmark.add_argument(
        "task_ids",
        nargs="*",
        type=int,
        help="ID of tasks to be unmarked"
    )

    label_delete = label_subparsers.add_parser(
        "delete",
        parents=[parent_parser],
        help="Delete label"
    )
    label_delete.add_argument(
        "label_id",
        type=int,
        help="Label ID"
    )

    return label_parser


@log_call_and_exception
def init_reminder_parser(parent_parser, subparsers):
    reminder_parser = subparsers.add_parser(
        "reminder",
        help="Reminder commands"
    )
    reminder_subparsers = reminder_parser.add_subparsers(dest="method")
    reminder_subparsers.required = True

    reminder_create = reminder_subparsers.add_parser(
        "create",
        parents=[parent_parser],
        help="Create reminder"
    )
    reminder_create.add_argument(
        "task_id",
        type=int,
        help="Task ID"
    )
    reminder_create.add_argument(
        "date",
        type=valid_datetime,
        help="Reminder date"
    )

    reminder_remove = reminder_subparsers.add_parser(
        "remove",
        parents=[parent_parser],
        help="Remove reminder"
    )
    reminder_remove.add_argument(
        "reminder_id",
        help="Reminder ID"
    )

    reminder_subparsers.add_parser(
        "list",
        parents=[parent_parser],
        help="List all reminders"
    )

    return reminder_parser


@log_call_and_exception
def init_rule_parser(parent_parser, subparsers):
    rule_parser = subparsers.add_parser("rule", help="Rule commands")
    rule_subparsers = rule_parser.add_subparsers(dest="method")
    rule_subparsers.required = True

    rule_create = rule_subparsers.add_parser(
        "create",
        parents=[parent_parser],
        help="Create repeat rule"
    )
    rule_create.add_argument(
        "task_id",
        type=int,
        help="ID of task to be repeated"
    )
    rule_create.add_argument(
        "every",
        type=valid_delta_type,
        help="Interval type(hour, day, month, year)"
    )
    rule_create.add_argument(
        "--quantity",
        type=int,
        nargs="?",
        const=1,
        default=1,
        help="Interval quantity"
    )
    rule_create.add_argument(
        "--end_count",
        type=int,
        help="Number of times to repeat task"
    )
    rule_create.add_argument(
        "--end_date",
        type=valid_datetime,
        help="End date of repeat rule"
    )

    rule_delete = rule_subparsers.add_parser(
        "remove",
        parents=[parent_parser],
        help="Remove repeat rule"
    )
    rule_delete.add_argument(
        "rule_id",
        type=int,
        help="Rule ID"
    )

    return rule_parser

@log_call_and_exception
def parse_args():
    parser = DefaultHelpParser(
        description="Simple todo app for your productivity."
    )
    subparsers = parser.add_subparsers(dest="subject")
    subparsers.required = True

    root_parser = argparse.ArgumentParser(add_help=False)
    root_parser.add_argument(
        "--user",
        help="Current user"
    )
    root_parser.add_argument(
        "--database",
        help="Database location"
    )

    init_task_parser(root_parser, subparsers)
    init_project_parser(root_parser, subparsers)
    init_label_parser(root_parser, subparsers)
    init_reminder_parser(root_parser, subparsers)
    init_rule_parser(root_parser, subparsers)

    args = parser.parse_args()
    get_cli_logger().debug(f"Input args: {args}")
    return args
