# TodoRonger

Simple todo app for your productivity.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

```
Python 3.6.5
```

### Installing

Clone the repository.

```
git clone https://ketrio@bitbucket.org/ketrio/todo-ronger.git
```

Install the app and dependencies.

```
cd todo-ronger
pip install .
```

Run the app and see help message.

```
todoronger -h
```


## Built With

* [SQLAlchemy](https://www.sqlalchemy.org/) - SQL toolkit and Object Relational Mapper
* [Dateutil](http://dateutil.readthedocs.io/en/stable/) - Useful extensions to the standard Python datetime features


## Config

You can also configure todoronger app by editing `todo_cli/config.py`

 * DB_TYPE - Database type
 * DB_PATH - Database location
 * LIB_LOGGER_FILE, CLI_LOGGER_FILE - Logger file location
 * LIB_LOGGER_LEVEL, CLI_LOGGER_LEVEL - Logger level
 * LIB_LOGGER_FORMAT, CLI_LOGGER_FORMAT - Logger message format
 * LIB_LOGGER_DATE_FORMAT, CLI_LOGGER_DATE_FORMAT - Logger message date format
 * WARNING_FILTER - Warning filter

## Library usage example

```
In [1]: from todo_lib.api import Api
   ...: from todo_lib.models import init_session
   ...:

In [2]: user = "Vlad"
   ...: session = init_session("sqlite", "/home/{username}/.todoronger/db.sqlite3") # initializing database session
   ...: api = Api(session) # inititalizing todo_lib api
   ...:

In [3]: # using save_after you don"t have to worry about saving changes,
   ...: # after execution it will save all the changes
   ...: # or discard them in case of exception
   ...: with api.save_after():
   ...:     work = api.create_project(user=user, name="Work")
   ...:     # create task and add it to project
   ...:     work.tasks.append(api.create_task(user=user, name="Check db configuration"))
   ...:     work.tasks.append(api.create_task(user=user, name="Write documentation"))
   ...:     work.tasks.append(api.create_task(user=user, name="Upload code on cloud"))  
   ...:     

In [4]: # loading project from database and checking its tasks
   ...: work = api.get_project(user=user, name="Work")
   ...: for task in work.tasks:
   ...:     print(task.name)
   ...:

Check db configuration
Write documentation
Upload code on cloud

```

## Console application usage example

```
username $ todoronger
error: the following arguments are required: subject
usage: todoronger [-h] {task,project,label,reminder,rule} ...

Simple todo app for your productivity.

positional arguments:
  {task,project,label,reminder,rule}
    task                Task commands
    project             Project commands
    label               Label commands
    reminder            Reminder commands
    rule                Rule commands

optional arguments:
  -h, --help            show this help message and exit
username $ todoronger project create work
Project created successfully! [1]
username $ todoronger task create "Check db configuration"
Task created successfully! [1]
username $ todoronger task create "Write documentation"
Task created successfully! [2]
username $ todoronger task create "Upload code on cloud"
Task created successfully! [3]
username $ todoronger project add 1 1 2 3
Tasks successfully added to project

[1] work
Tasks: 3
username $ todoronger project show 1
[1] work
Tasks: 3

[1] Check db configuration
description: None
completed: False
date: None
labels: []
project: work

[2] Write documentation
description: None
completed: False
date: None
labels: []
project: work

[3] Upload code on cloud
description: None
completed: False
date: None
labels: []
project: work


```
